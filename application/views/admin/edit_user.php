<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      	<div class="container-fluid">
        	<div class="row mb-2">
          	<div class="col-sm-6">
            	<h1 class="m-0">Team</h1>
          	</div><!-- /.col -->
          	<div class="col-sm-6">
            	<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="<?php echo base_url('dashboard'); ?>">Home</a></li>
					<li class="breadcrumb-item active">Teams</li>
            	</ol>
          	</div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            <div class="col-sm-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Team Member</h3>
                    </div>
            
                    <!-- form start -->
                    <form method="post" action="<?= site_url('Teams/update_user') ?>">

                        <input type="hidden" name="id" value="<?= $userslist[0]['id'] ?>" />
                        <input type="hidden" name="old_role" value="<?= $userslist[0]['position_type'] ?>" />

                        <div class="card-body team-form">
                            <?php
                            if($this->session->flashdata('response')){
                                echo $this->session->flashdata('response');
                                $this->session->unset_userdata('response');
                            } ?>
                            <div class="err_datasse"><?= validation_errors() ?></div>
                            <div class="form-group">
                                <label>Name</label>
                                <input
                                    type="text"
                                    value="<?= $userslist[0]['name'] ?>"
                                    required
                                    disabled
                                    name="name"
                                    class="form-control"
                                    placeholder="Enter Name"
                                />
                            </div>
                            <div class="form-group">
                                <label>Select Position</label>
                                <select required name="position_type" class="form-control">
                                    <?php
									foreach (accessLavels($this->userlogin_type) as $key => $value) {
                                        $selected='';
                                        if($userslist[0]['position_type'] == $key) {
                                            $selected = 'selected ';
                                        }
                                        echo '<option '.$selected.' value="'.$key.'">'.$value.'</option>';
									}
									?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Email address</label>
                                <input
                                    type="email"
                                    value="<?= $userslist[0]['email'] ?>"
                                    required
                                    disabled
                                    name="email"
                                    class="form-control"
                                    placeholder="Enter email"
                                />
                            </div>
                            <div class="form-group">
                                <label>Mobile Number</label>
                                <input
                                    type="number"
                                    value="<?= $userslist[0]['mobile'] ?>"
                                    required
                                    disabled
                                    name="mobile"
                                    class="form-control"
                                    placeholder="Enter Mobile"
                                />
                            </div>
                                        
                            <div class="form-group">
                                <label>
                                    New Assignee
                                    <a
                                        href="#"
                                        data-toggle="tooltip"
                                        data-placement="top"
                                        title="Select member to transfer the work from <?= $userslist[0]['name'] ?>"
                                    ><i class="ion ion-eye"></i></a>
                                </label>
                                <select required name="new_assignee" class="form-control">
                                    <option value="">Select</option>
                                    <?php
                                    foreach ($newAssignee as $key => $value) {
                                        
                                        if($value['id'] != $userslist[0]['id']) {

                                            echo "<option value='".$value['id']."'>".
                                                    $value['name']." (".$value['email'].")".
                                                "</option>";
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                
                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
        </div>
        <!-- /.row (main row) -->
    </div>
</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>