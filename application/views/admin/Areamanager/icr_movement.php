<?php
if (isset($id)) {
    $id = $id;
    $required = '';
    $filled_signed_file = '<a href="'.base_url('assets/project_document/').$filled_signed_file.'" target="__blank">View</a>';
    $hamipatra_file = '<a href="'.base_url('assets/project_document/').$hamipatra_file.'" target="__blank">View</a>';
    $payment_advice_file = '<a href="'.base_url('assets/project_document/').$payment_advice_file.'" target="__blank">View</a>';
} else {
    $required = 'required="required"';
    $id = $filled_signed_file = $hamipatra_file = $payment_advice_file = '';
}

$limeman_sign_date = isset($limeman_sign_date) ? $limeman_sign_date : '';
$ae_je_sign_date = isset($ae_je_sign_date) ? $ae_je_sign_date : '';
$inward_date = isset($inward_date) ? $inward_date : '';
$ro_date = isset($ro_date) ? $ro_date : '';
$do_date = isset($do_date) ? $do_date : '';
$ho_date = isset($ho_date) ? $ho_date : '';
$act_dept_date = isset($act_dept_date) ? $act_dept_date : '';
$se_tbl_date = isset($se_tbl_date) ? $se_tbl_date : '';
$moved_to_ho_date = isset($moved_to_ho_date) ? $moved_to_ho_date : '';
$invoice_date = isset($invoice_date) ? $invoice_date : '';
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">ICR Movement</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Project Head</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Main row -->
            <div class="row" style="display:block;">
                <!-- form start -->
                <form method="post" enctype="multipart/form-data" action="<?= base_url('Areamanager/add_icr_movement') ?>">
                    <?php
                    if($this->session->flashdata('response')){
                        echo $this->session->flashdata('response');
                        $this->session->unset_userdata('response');
                    } ?>
                    <div class="err_datasse">
                        <?= validation_errors() ?>
                    </div>

                    <input type="hidden" name="site_id" value="<?= $site_id ?>" />
                    <input type="hidden" name="id" value="<?= $id ?>" />
                    <input type="hidden" name="project_id" value="<?= $project_id ?>" />

                <div class="card-body team-form">
                    <div class=" card card-danger">
                        <div class="card-header">
                            <h3 class="card-title">ICR Movement</h3>
                        </div>
                        <div class="card-body">
                            <?php
                            $get_ICR_form_fields = get_ICR_form_fields();
                            
                            // echo "<pre>"; print_r($ICR_form_fields);
                            // echo "<pre>"; print_r($get_ICR_form_fields); echo "</pre>"; die;

                            foreach ($ICR_form_fields as $key => $value) {
                                if ($get_ICR_form_fields[$value]['type'] == 'file') {
                                    echo '<div class="form-group">
                                        <label>'.$get_ICR_form_fields[$value]['title'].'</label>
                                        <div class="custom-file">
                                            <input 
                                                type="file" 
                                                class="custom-file-input"
                                                name="'.$get_ICR_form_fields[$value]['name'].'" '.
                                                $required.'
                                            />
                                            <label class="custom-file-label" for="customFile">Choose file</label>
                                            '.${$get_ICR_form_fields[$value]['name']}.'
                                        </div>
                                    </div>';
                                } else {
                                    if (empty(${$get_ICR_form_fields[$value]['name']})) {
                                        $inp_val = '';
                                    } else {
                                        $inp_val = ${$get_ICR_form_fields[$value]['name']};
                                    }

                                    echo '<div class="form-group">
                                        <label>'.$get_ICR_form_fields[$value]['title'].'</label>
                                        <input 
                                            type="'.$get_ICR_form_fields[$value]['type'].'" 
                                            class="form-control" 
                                            name="'.$get_ICR_form_fields[$value]['name'].'" 
                                            value="'.$inp_val.'" required />
                                    </div>';
                                }
                            }  
                            ?>
						</div>
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Complete</button>
                      </div>
                </div>
                <!-- /.card -->


                <!-- /.col (right) -->
            </div>

        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>