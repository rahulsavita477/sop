<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Create Form</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Create Form</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
        
        <!-- Main row -->
        <div class="row">
            <div class="col-sm-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Form Creation</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    
                    <form method="post" action="<?= base_url('Forms/add_form') ?>">
                        
                        <?php
                        if($this->session->flashdata('response')){
                            echo $this->session->flashdata('response');
                            $this->session->unset_userdata('response');
                        } ?>
                        
                        <div class="err_datasse">
                            <?php echo validation_errors(); ?>
                        </div>

                        <div class="card-body">
                            <div class="form-group">
                                <label>Field Title</label>
                                <input type="text" name="title" required class="form-control" placeholder="Field Title" />
                            </div>

                            <div class="form-group">
                                <label>Field Name <span style="font-weight: bold; font-size: 10px; color: red;">(Only alphabets and underscore allowed, Should be unique)</span></label>
                                <input type="text" name="name" required class="form-control" placeholder="Field Name" />
                            </div>

                            <div class="form-group">
                                <label>Field Type</label>
                                <select name="type" required class="form-control">

                                    <?php foreach($field_type_enum as $list) {
                                        echo "<option>".$list."</option>";
                                    } ?>

                                </select>
                            </div>
          
                            <div class="form-group">
                                <label>Field Min Value</label>
                                <input type="number" name="min" class="form-control" placeholder="Field Min Value" />
                            </div>
          
                            <div class="form-group">
                                <label>Field Max Value</label>
                                <input type="number" name="max" class="form-control" placeholder="Field Max Value" />
                            </div>

                            <div class="form-group">
                                <label>Field Order</label>
                                <input type="number" name="field_order" required class="form-control" placeholder="Field Order" />
                            </div>

                            <div class="form-group">
                                <label>Form Type</label>
                                <select name="form" required class="form-control">

                                    <?php foreach($form_type_enum as $list) {
                                        echo "<option>".$list."</option>";
                                    } ?>

                                </select>
                            </div>

                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
        </div>
        <!-- /.row (main row) -->
    
        <div class="row">
            <div class="col-sm-12">
                <div class="team-table"></div>
            </div>
        </div>
    
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>