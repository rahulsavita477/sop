<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

<div class="content-wrapper">
    
    <!-- Content Header (Page header) -->
    <div class="content-header">
      	<div class="container-fluid">
        	<div class="row mb-2">
          		<div class="col-sm-6">
            		<h1 class="m-0">Forms List</h1>
          		</div><!-- /.col -->
          
          		<div class="col-sm-6">
            		<ol class="breadcrumb float-sm-right">
              			<li class="breadcrumb-item"><a href="#">Home</a></li>
              			<li class="breadcrumb-item active">Forms</li>
            		</ol>
          		</div><!-- /.col -->
        	</div><!-- /.row -->
      	</div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      	<div class="container-fluid">
        	<!-- Main row -->
        	<div class="row">
				<div class="col-sm-12 gridDiv">
					<?php
					if($this->session->flashdata('response')){
						echo $this->session->flashdata('response');
						$this->session->unset_userdata('response');
					} ?>
					<div class="team-table">
						<table id="example1" class="table table-bordered table-striped datatable_sets">
                  			<thead>
                  				<tr>
									<th>Title</th>
									<th>Name</th>
									<th>Type</th>
									<th>MIN</th>
									<th>MAX</th>
									<th>Order</th>
									<th>Form</th>
									<th>Action</th>
                  				</tr>
                  			</thead>
                  			<tbody>
                  				<?php
                  				foreach ($forms as $key => $value) {
									
                  					echo "<tr>
											<td>".$value['title']."</td>
											<td>".$value['name']."</td>
											<td>".$value['type']."</td>
											<td>".$value['min']."</td>
											<td>".$value['max']."</td>
											<td>".$value['field_order']."</td>
											<td>".$value['form']."</td>
											<td>
												<a href='".base_url("/Forms/edit/".$value['id'])."' class='btn btn-primary'>Edit</a>
											</td>
	                  					</tr>";
                  				}
                  				?>
				  			</tbody>
				  		</table>
					</div>
				</div>
			</div>
		</div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
