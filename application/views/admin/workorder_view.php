<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?= base_url('assets/') ?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0">Order</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Operation Head</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<?php if($this->userlogin_type != ADMIN) { ?>
				<!-- Main row -->
				<div class="row">
					<div class="col-sm-12">
						<!-- general form elements -->
						<div class="card card-primary">
							<div class="card-header">
								<h3 class="card-title"><b>Submit Order Details</h3>
							</div>
							<!-- /.card-header -->

							<!-- form start -->
							<form method="post" action="<?= site_url('Workorder/add_order') ?>">
								<div class="card-body team-form">
									<?php
									if($this->session->flashdata('response')){
										echo $this->session->flashdata('response');
										$this->session->unset_userdata('response');
									} ?>

									<div class="form-group">
										<label>LOI Number</label>
										<input type="text" name="loi_number" class="form-control" required />
									</div>
									<div class="form-group">
										<label>LOI Date</label>
										<div class="input-group date reservationdate" id="reservationdate"
											data-target-input="nearest">
											<input type="text" name="loi_date" class="form-control datetimepicker-input"
												data-target="#reservationdate">
											<div class="input-group-append" data-target="#reservationdate"
												data-toggle="datetimepicker">
												<div class="input-group-text"><i class="fa fa-calendar"></i></div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label>Work Order Number</label>
										<input type="text" name="workorder_no" class="form-control" required />
									</div>
									<div class="form-group">
										<label>Work Order Date</label>
										<div class="input-group date reservationdate" id="reservationdate1"
											data-target-input="nearest">
											<input type="text" name="workorder_date"
												class="form-control datetimepicker-input" data-target="#reservationdate1"  required />
											<div class="input-group-append" data-target="#reservationdate1"
												data-toggle="datetimepicker">
												<div class="input-group-text"><i class="fa fa-calendar"></i></div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label>Work Order Deadline</label>
										<input type="date" name="order_deadline" class="form-control" required />
									</div>
									<div class="form-group">
										<label>Assign to Project Head</label>
										<div class="select2-purple">
											<select class="select2" name="project_head[]" multiple="multiple"
												data-placeholder="Select" data-dropdown-css-class="select2-purple"
												style="width: 100%;" required="required">
												<?php foreach($project_heads as $list) { ?>
												<option value="<?= $list['id']; ?>">
													<?= $list['name']; ?>
												</option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label>Project</label>
										<div class="select2-purple">
											<select required name="project_id" class="form-control">
												<option value=''>Select</option>
												<?php foreach($projects as $list) { ?>
													<option value="<?= $list['id'] ?>"> <?= $list['project_name'] ?> </option>
												<?php } ?>
											</select>
										</div>
									</div>
								</div>
								<!-- /.card-body -->

								<div class="card-footer">
									<button type="submit" class="btn btn-primary">Submit Order Details</button>
								</div>
							</form>
							<div class="content-header">
								<!-- /.container-fluid -->
							</div>
						</div>
						<!-- /.card -->
					</div>
				</div>
				<!-- /.row (main row) -->
			<?php } ?>

			<div class="container-fluid">
				<div class="row mb-2">
					<div class="col-sm-6">
						<h2 class="m-0">Recent Orders</h2>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="team-table">
						<table class="table table-bordered table-striped datatable_sets">
							<thead>
								<tr>
									<th>LOI Number</th>
									<th>LOI Date</th>
									<th>Work Order Number</th>
									<th>Work Order Date</th>
									<th>Work Order Deadline</th>
									<th>Project Name</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach($workorder_list as $list) { ?>
								<tr>
									<td><?= $list['loi_no'] ?></td>
									<td><?= $list['loi_date'] ?></td>
									<td><?= $list['workorder_no'] ?></td>
									<td><?= $list['workorder_date'] ?></td>
									<td><?= $list['order_deadline'] ?></td>
									<td><?= $list['project_name'] ?></td>
									<td>
										<a href="<?= base_url('workorder/edit_order/'.$list['id']); ?>" class="btn btn-block btn-success">Edit</a>
									</td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
