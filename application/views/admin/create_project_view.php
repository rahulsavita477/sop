<?php
$project_name = isset($prj_details['project_name']) ? $prj_details['project_name'] : '';
?>

<!-- Select2 -->
<link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0">Create Project</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active">Project Head</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
        
        <!-- Main row -->
        <div class="row">
            <div class="col-sm-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Project Creation</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    
                    <form method="post" enctype="multipart/form-data" action="<?php echo base_url('Projects/add_project'); ?>">
                        <?php
                        if($this->session->flashdata('response')){
                            echo $this->session->flashdata('response');
                            $this->session->unset_userdata('response');
                        } ?>
                        <div class="err_datasse">
                            <?php echo validation_errors(); ?>
                        </div>

                        <input type="hidden" name="workorder_id" value="<?= $workorder_id ?>" />

                        <div class="card-body">
                            <div class="form-group">
                                <label>Select State</label>
                                <select name="state" required id="state" class="form-control">
                                    <?php foreach($statelists as $list) {
                                        if (isset($prj_details['state']) && $prj_details['state'] == $list['name']) {
                                            $selected = "selected='selected'";
                                        } else {
                                            $selected = '';
                                        }

                                        echo "<option ".$selected.">".$list['name']."</option>";
                                    } ?>
                                </select>
                            </div>
          
                            <div class="form-group">
                                <label for="exampleInputEmail1">Project Name</label>
                                <input type="text" name="project_name" required class="form-control" id="exampleInputName1" placeholder="Project Name" value="<?= $project_name ?>" />
                            </div>
          
                            <!-- <div class="form-group">
                                <label for="exampleInputMobile1">Defining project Manager</label>
                                <div class="select2-purple">
                                    <select class="select2" required name="project_manager[]" multiple="multiple" data-placeholder="Select" data-dropdown-css-class="select2-purple" style="width: 100%;">

                                        <?php 
                                        $project_manager_ids = isset($prj_details['project_manager']) ? explode(",", $prj_details['project_manager']) : [];

                                        foreach($project_managers as $manager) {
                                            if (in_array($manager['id'], $project_manager_ids)) {
                                                $selected = "selected='selected'";    
                                            } else {
                                                $selected = "";
                                            }

                                            echo "<option ".$selected." value='".$manager['id']."'>".$manager['name']." (Project Manager)</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div> -->
          
                            <div class="form-group">
                                <label for="exampleInputEmail1">Defining phase wise perameters</label>
                                <select name="define_phase" required class="form-control">
                                    <?php for($ij=1; $ij<=15; $ij++) {
                                        if (isset($prj_details['define_phase']) && $prj_details['define_phase'] == $ij) {
                                            $selected = "selected='selected'";
                                        } else {
                                            $selected = '';
                                        }

                                        echo "<option ".$selected.">".$ij."</option>";
                                    }
                                    ?>
                                </select>
                            </div>
          
                            <div class="form-group">
                                <label for="exampleInputFile">
                                    <?php 
                                    if(isset($prj_details['id'])) {
                                        
                                        echo 'Upload sites (excel Formate - csv)
                                            <a href="'.base_url('/assets/project_document/'.$prj_details['site_file']).'">Excel</a>';
                                        
                                    } else {
                                        echo 'Upload sites (excel Formate - csv) *';
                                    }
                                    ?>
                                </label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <?php 
                                        if(isset($prj_details['id'])) {
                                        
                                            echo '<input type="file" accept=".csv" name="site_file" required class="custom-file-input" id="exampleInputFile">';
                                            
                                        } else {

                                            echo '<input type="file" accept=".csv" name="site_file" class="custom-file-input" id="exampleInputFile">';
                                        }

                                        ?>
                                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                    </div>
                                    <div class="input-group-append">
                                        <span class="input-group-text">Upload</span>
                                    </div>
                                </div>
                            </div>
          
                            <div class="form-group">
                                <label for="exampleInputFile">Upload / Create Gathering Form Format</label>
                                <div class="input-group">
                                    <select class="select2" required name="gathering_form_fields[]" multiple="multiple" data-placeholder="Select" data-dropdown-css-class="select2-purple" style="width: 100%;">
                                        <?php
                                        if (isset($prj_details['gathering_form_fields'])) {
                                            $gathering_form_fields = json_decode($prj_details['gathering_form_fields']);
                                        } else {
                                            $gathering_form_fields = [];
                                        }

                                        foreach ($gathering_fields as $value) {
                                            if (in_array($value['id'], $gathering_form_fields)) {
                                                $selected = "selected='selected'";    
                                            } else {
                                                $selected = "";
                                            }

                                            echo "<option ".$selected." value='".$value['id']."'>".$value['title']."</option>"; 
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputFile">Upload / execution Form Format</label>
                                <div class="input-group">
                                    <select class="select2" required name="execution_form_fields[]" multiple="multiple" data-placeholder="Select" data-dropdown-css-class="select2-purple" style="width: 100%;">
                                        <?php
                                        if (isset($prj_details['execution_form_fields'])) {
                                            $execution_form_fields = json_decode($prj_details['execution_form_fields']);
                                        } else {
                                            $execution_form_fields = [];
                                        }

                                        foreach ($execution_fields as $value) {
                                            if (in_array($value['id'], $execution_form_fields)) {
                                                $selected = "selected='selected'";    
                                            } else {
                                                $selected = "";
                                            }

                                            echo "<option ".$selected." value='".$value['id']."'>".$value['title']."</option>"; 
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputFile">Upload / Create Survey Form Format</label>
                                <div class="input-group">
                                    <select class="select2" required name="survey_form_fields[]" multiple="multiple" data-placeholder="Select" data-dropdown-css-class="select2-purple" style="width: 100%;">
                                        <?php
                                        if (isset($prj_details['survey_form_fields'])) {
                                            $survey_form_fields = json_decode($prj_details['survey_form_fields']);
                                        } else {
                                            $survey_form_fields = [];
                                        }
                                        
                                        foreach ($survey_fields as $value) {
                                            if (in_array($value['id'], $survey_form_fields)) {
                                                $selected = "selected='selected'";    
                                            } else {
                                                $selected = "";
                                            }

                                            echo "<option ".$selected." value='".$value['id']."'>".$value['title']."</option>"; 
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">      
                                <label for="exampleInputFile">Upload / Create ICR Form Format</label>
                                <div class="input-group">
                                    <select class="select2" required name="ICR_form_fields[]" multiple="multiple" data-placeholder="Select" data-dropdown-css-class="select2-purple" style="width: 100%;">
                                      <?php
                                        if (isset($prj_details['ICR_form_fields'])) {
                                            $ICR_form_fields = json_decode($prj_details['ICR_form_fields']);
                                        } else {
                                            $ICR_form_fields = [];
                                        }
                                        
                                        foreach ($icr_fields as $value) {
                                            if (in_array($value['id'], $ICR_form_fields)) {
                                                $selected = "selected='selected'";    
                                            } else {
                                                $selected = "";
                                            }

                                            echo "<option ".$selected." value='".$value['id']."'>".$value['title']."</option>"; 
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                  
                            <div class="form-group">
                                <label for="exampleInputEmail1">Scope Of Work With Deadlines</label>
                                <textarea class="form-control" name="work_deadlines" rows="3" placeholder="Enter ..."><?= isset($prj_details['scope_deadlines']) ? $prj_details['scope_deadlines'] : '' ?></textarea>
                            </div>
                        </div>
                        <!-- /.card-body -->

                        <div class="card-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
                <!-- /.card -->
            </div>
        </div>
        <!-- /.row (main row) -->
    
        <div class="row">
            <div class="col-sm-12">
                <div class="team-table"></div>
            </div>
        </div>
    
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>