<?php
$userlogin_type = $this->session->userdata('ses_userlogin_type')
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      	<div class="container-fluid">
        	<div class="row mb-2">
          	<div class="col-sm-6">
            	<h1 class="m-0">Team</h1>
          	</div><!-- /.col -->
          	<div class="col-sm-6">
            	<ol class="breadcrumb float-sm-right">
					<li class="breadcrumb-item"><a href="<?= base_url('dashboard'); ?>">Home</a></li>
					<li class="breadcrumb-item active">Teams</li>
            	</ol>
          	</div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">

		<?php
		if($this->session->flashdata('response')) {

			echo $this->session->flashdata('response');
			$this->session->unset_userdata('response');
		}
		?>

		<!-- Main row -->
		<div class="row">
			<div class="col-sm-12">
				<!-- general form elements -->
				<div class="card card-primary">
					<div class="card-header">
						<h3 class="card-title">Add Team Member</h3>
					</div>
			
					<!-- form start -->
					<form method="post" action="<?= site_url('Teams/add_team') ?>">
						<div class="card-body team-form">
							<div class="err_datasse"><?= validation_errors() ?></div>
							<div class="form-group">
								<label>Name</label>
								<input
									type="text"
									value="<?= set_value('name') ?>"
									required name="name"
									class="form-control"
									placeholder="Enter Name"
								/>
							</div>
							<div class="form-group">
								<label>Select Position</label>
								<select required="" name="position_type" class="form-control">
									<option value="">Select</option>
									<?php
									foreach (accessLavels($this->userlogin_type) as $key => $value) {
										echo '<option value="'.$key.'">'.$value.'</option>';
									}
									?>
								</select>
							</div>
							<div class="form-group">
								<label>Email address</label>
								<input
									type="email"
									value="<?= set_value('email') ?>"
									name="email"
									class="form-control"
									placeholder="Enter email"
									required
								/>
							</div>
							<div class="form-group">
								<label>Mobile Number</label>
								<input
									type="number"
									value="<?= set_value('mobile') ?>"
									name="mobile"
									class="form-control"
									placeholder="Enter Mobile"
								/>
							</div>
						</div>
				
						<div class="card-footer">
							<button type="submit" class="btn btn-primary">Submit</button>
						</div>
					</form>
				</div>
				<!-- /.card -->
			</div>
		</div>
		<!-- /.row (main row) -->

		<div class="row">
			<div class="col-sm-12">
				<div class="team-table">
				<table description="" class="table table-bordered table-striped datatable_sets">
                  	<thead>
                  		<tr>
							<th>Action</th>
							<th>Name</th>
							<th>Position</th>
							<th>Email</th>
							<th>Mobile Number</th>
							<th>User Id</th>
							<th>Password</th>
							<th>Status</th>
						</tr>
                  	</thead>
                  	<tbody>
				  		<?php
						foreach($userslist as $list)
						{
							$role = $list['position_type'];
							$role = ucfirst(str_replace("_", " ", $role));
				  		?>
                  		<tr>
						  	<td>
								<a href="<?= base_url('Teams/edit/'.$list['id']) ?>">Edit</a>/
								<a href="<?= base_url('Teams/delete_user/'.$list['id']) ?>">Delete</a>
							</td>
							<td><?= $list['name'] ?></td>
							<td>
								<?= $role ?>
							</td>
							<td><?= $list['email'] ?></td>
							<td><?= $list['mobile'] ?></td>
							<td><?= $list['user_id'] ?></td>
							<td><?= $list['password'] ?></td>
							<td>
								<?php
								if($list['status']==1) { ?>

									<a
										onclick="return confirm('Are you sure you want to deactivate this account?');"
										href="<?= base_url('Teams/deactivate_team/'.$list['id']) ?>"
										class="btn btn-block btn-success"
									>Activate</a>
								<?php } else { ?>

									<a
										onclick="return confirm('Are you sure you want to activate this account?');"
										href="<?= base_url('Teams/activate_team/'.$list['id']) ?>" class="btn btn-block btn-danger"
									>Deactivate</a>
							
								<?php } ?>
							</td>
                  		</tr>
				 	<?php } ?>
				</tbody>
			</table>
		</div>
	</div>
</div>
		
</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Modal Header</h4>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>
