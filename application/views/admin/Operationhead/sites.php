<link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Site List (<?= $project_detail['state']; ?> / <?= $project_detail['project_name']; ?> / <?= count($sites_list); ?>)</h1>

            <form action="" method="GET">
	            <select name="circle">
	            	<option value="0">Select Circle</option>
	            	<?php
	            	foreach ($circle_list as $key => $value) {
	            		if (isset($_GET['circle']) && $_GET['circle'] == $value['circle_name']) {
	            			$option_selected = "selected='selected'";
	            		} else {
	            			$option_selected = "";
	            		}
	            		
	            		echo "<option name='".$value['circle_name']."' ".$option_selected.">".$value['circle_name']."</option>";
	            	}
	            	?>
	            </select>

	            <select name="district">
	            	<option value="0">Select District</option>
	            	<?php
	            	foreach ($district_list as $key => $value) {
	            		if (isset($_GET['district']) && $_GET['district'] == $value['land_district']) {
	            			$option_selected = "selected='selected'";
	            		} else {
	            			$option_selected = "";
	            		}
	            		
	            		echo "<option name='".$value['land_district']."' ".$option_selected.">".$value['land_district']."</option>";
	            	}
	            	?>
	            </select>

	            <select name="taluka">
	            	<option value="0">Select Taluka</option>
	            	<?php
	            	foreach ($taluka_list as $key => $value) {
	            		if (isset($_GET['taluka']) && $_GET['taluka'] == $value['land_taluka']) {
	            			$option_selected = "selected='selected'";
	            		} else {
	            			$option_selected = "";
	            		}
	            		
	            		echo "<option name='".$value['land_taluka']."' ".$option_selected.">".$value['land_taluka']."</option>";
	            	}
	            	?>
	            </select>

	            <select name="village">
	            	<option value="0">Select Village</option>
	            	<?php
	            	foreach ($village_list as $key => $value) {
	            		if (isset($_GET['village']) && $_GET['village'] == $value['land_village']) {
	            			$option_selected = "selected='selected'";
	            		} else {
	            			$option_selected = "";
	            		}
	            		
	            		echo "<option name='".$value['land_village']."' ".$option_selected.">".$value['land_village']."</option>";
	            	}
	            	?>
	            </select>

	            <input type="submit" value="Get Result">
	            <a href="<?= strtok($_SERVER["REQUEST_URI"], '?') ?>">Remove Filter</a>
            </form>
          </div><!-- /.col -->
          <div class="col-sm-6">
		    
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        
        <!-- Main row -->
        <div class="row">
			<div class="col-sm-12 gridDiv">
				<?php
				if($this->session->flashdata('response')){
					echo $this->session->flashdata('response');
					$this->session->unset_userdata('response');
				} ?>
				<div class="team-table">
					<table id="example1" class="table table-bordered table-striped datatable_sets_server">
                  <thead>
                  <tr>
					<th>Number</th>
                    <th>Circle Name</th>
					<th>Land District</th>
					<th>Land Village</th>
					<th>Land Taluka</th>
					<th>WORKORDER NO</th>
					<th>Beneficiary Id</th>
					<th>Beneficiary Name</th>
					<th>Mobile Number</th>
					<th>Land Address</th>
                    <th>Pump Load</th>
                    <th>Category</th>
                    <th>Work Order Dt</th>
                    <th>Application Status</th>
					<th>Installation Status</th>
					<th>Installation Date</th>
					<th>Remarks</th>
					<th>Lot</th>
					<th>Site Engineers</th>
					<th>Area Manager</th>
					<th>Contractor</th>
					<th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
				  <?php 
				  /*
				  $ii=0;
				  foreach($sites_list as $list)
				  {
					  $ii++;
				  ?>
                  <tr>
                    <td><?= $ii; ?></td>
					<td><?= $list['circle_name']; ?></td>
					<td><?= $list['land_district']; ?></td>
					<td><?= $list['land_village']; ?></td>
					<td><?= $list['land_taluka']; ?></td>
					<td><?= $list['workorder_no']; ?></td>
					<td><?= $list['beneficiary_id']; ?></td>
					<td><?= $list['beneficiary_name']; ?></td>
					<td><?= $list['mobilen_number']; ?></td>
					<td><?= $list['land_address']; ?></td>
					<td><?= $list['pump_load']; ?></td>
					<td><?= $list['category']; ?></td>
					<td><?= $list['work_order_date']; ?></td>
					<td><?= $list['application_status']; ?></td>
					<td><?= $list['installation_status']; ?></td>
					<td><?= $list['installation_date']; ?></td>
					<td><?= $list['remarks']; ?></td>
					<td><?= $list['lot']; ?></td>                    
                  </tr>
				 <?php 
				  }
				  */
				  ?>
				  
				  </tbody>
				  </table>
				</div>
			</div>
		</div>
		


		
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 <script>
$(function() {
	// get query string parameters
	function getQueryParamByName(name) {
		// url = window.location.href
	    name = name.replace(/[\[\]]/g, '\\$&');
	    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
	        results = regex.exec(window.location.href);
	    if (!results) return null;
	    if (!results[2]) return '';
	    return decodeURIComponent(results[2].replace(/\+/g, ' '));
	}

	let url = "<?= base_url("Workorder/sites_server/".$project_detail['id']) ?>?circle="+getQueryParamByName('circle')+"&district="+getQueryParamByName('district')+"&taluka="+getQueryParamByName('taluka')+"&village="+getQueryParamByName('village')+"&filter="+getQueryParamByName('filter');

	var table_news = $('.datatable_sets_server').DataTable({
		"processing":true,
		"serverSide":true,  
		'searching': false,
		"ordering": false,
		//"responsive": true,
		"order":[], 
		"ajax": {
			url : url,
			type: "GET"  
		},
		"columnDefs":[  
			{  
				"targets":[0],  
				"orderable":false,
			},  
		], 
	});
});
 </script>