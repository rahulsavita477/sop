<?php
$userLoginType = $this->session->userdata('ses_userlogin_type');

if($isApproved == 1 && $userLoginType == CONTRACTOR) {
	$disabled = " disabled";
} else {
	$disabled = "";
}
?>

<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0">Edit Site Data</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Edit Site</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Main row -->
			<div class="row">
				<div class="col-sm-12">
					<!-- general form elements -->
					<div class="card card-primary">
						<div class="card-header">
							<h3 class="card-title"><b>Site Data Gathering</b></h3>
						</div>
						<!-- /.card-header -->
						<!-- form start -->
						<form method="POST" enctype='multipart/form-data'
							action="<?php echo base_url('Siteengineer/updatesite/'.$id); ?>">
							<div class="card-body team-form">
								<?php
								// $get_gathering_form_fields = get_gathering_form_fields();
								
								if (count($gathering_form_fields) > 0) {

								echo "<input type='hidden' name='project_id' value='".$project_id."' />";

								$site_name = $beneficiary_name;

								foreach ($gathering_form_fields as $value) {
									
									// echo "<pre>"; print_r($gathering_form_fields); die; 
									$type = strtolower($value['type']);

									if ($type == 'file') {

										if(empty($id) || !isset($form_file_field_values[$value['name']])) {

											$required = "required='required'";
											$img='';
										} else {
											
											$img = '<a href="'.base_url('assets/project_document/').$form_file_field_values[$value['name']].'" target="__blank">View</a>';
											$required='';
										}

										echo '<div class="form-group">
											<label>'.$value['title'].'</label>
											<div class="custom-file">
												<input
													type="file"
													class="custom-file-input"
													name="'.$value['name'].'" '.
													$required.$disabled.'
												/>
												<label class="custom-file-label" for="customFile">Choose file</label>
												'.$img.'
											</div>
										</div>';
									}
									else {
										echo '<div class="form-group">
											<label>'.$value['title'].'</label>
											<input
												type="'.$type.'"
												class="form-control" 
												name="'.$value['name'].'" 
												value="'.(isset($form_field_value[$value['name']]) ? $form_field_value[$value['name']] : '').'"
												'.($type == "number" ? 
												'min="'.$value['min'].'" max="'.$value['max'].'"' : '') .'
												required
												'.$disabled.'
											/>
										</div>';
									}
								}
								}
								?>
							</div>
							<!-- /.card-body -->

							<div class="card-footer">
								<?php
								if($userLoginType != ADMIN && $disabled == '') {
									echo '<button type="submit" class="btn btn-primary">Update</button>';
								}
								?>
							</div>
						</form>
					</div>
					<!-- /.card -->
				</div>
			</div>
			<!-- /.row (main row) -->

			<div class="row">
				<div class="col-sm-12">
					<div class="team-table">

					</div>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
