<?php
$userLoginType = $this->session->userdata('ses_userlogin_type');

if (isset($id)) {

    $id = $id;
    $required = '';

} else {

    $required = 'required="required"';
    $id = '';
}

if($isApproved == 1 && $userLoginType == CONTRACTOR) {
	$disabled = " disabled";
} else {
	$disabled = "";
}
?>

<body class="hold-transition sidebar-mini layout-fixed">
  <div class="wrapper">

    <!-- Preloader -->
    <div class="preloader flex-column justify-content-center align-items-center">
      <img class="animation__shake" src="dist/img/logo.png" alt="span pumps" height="60" width="60">
    </div>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <div class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
      <!-- /.content-header -->

      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">

          <!-- Main row -->
          <div class="row">

            <div class="col-sm-12">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title"><b>Contractor Execution</b></h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form method="post" enctype="multipart/form-data" action="<?= base_url('Siteengineer/add_contractor_execution') ?>">
                    
					<?php
                    if($this->session->flashdata('response')) {

						echo $this->session->flashdata('response');
						$this->session->unset_userdata('response');
                    }
					?>

                    <div class="err_datasse">
                        <?= validation_errors() ?>
                    </div>

                    <input type="hidden" name="site_id" value="<?= $site_id ?>" />
                    <input type="hidden" name="project_id" value="<?= $project_id ?>" />
                    <input type="hidden" name="id" value="<?= $id ?>" />

                  <div class="card-body team-form">

                    <?php
                    if (count($execution_form_fields)>0) {

                      foreach ($execution_form_fields as $value) {

                          // echo "<pre>"; print_r($get_execution_form_fields[$value]); echo "</pre>"; die;
                          
                          $type = strtolower($value['type']);

                          if ($type == 'file') {

                            echo '<div class="form-group">
                                  <label>'.$value['title'].'</label>
                                  <div class="custom-file">
                                      <input 
                                          type="file" 
                                          class="custom-file-input"
                                          name="'.$value['name'].'[]" '.
                                          $required.$disabled.'
                                          multiple
                                      />
                                      <label class="custom-file-label" for="customFile">Choose file</label>';

							if(isset($form_file_field_values)) {
								
								foreach ($form_file_field_values[$value['name']] as $key => $value) {
									echo '<a href="'.base_url('assets/project_document/').$value.'" target="__blank">View</a>'.$key.', &nbsp;';
								}
							}
                            
                            echo '</div>
                              </div>';
                          }
                          else {
                              echo '<div class="form-group">
                                  <label>'.$value['title'].'</label>
                                  <input
                                      type="'.$type.'"
                                      class="form-control"
                                      name="'.$value['name'].'"
                                      value="'.(isset($form_field_value[$value['name']]) ? $form_field_value[$value['name']] : '').'" 
                                      '.($type == "number" ? 
                                        'min="'.$value['min'].'" max="'.$value['max'].'"' : '') .'
                                      required
									  '.$disabled.'
									/>
                              </div>';
                          }
                      }
                    }
                    ?>

                  </div>
                  <!-- /.card-body -->

                  <div class="card-footer">
                    <?php
					if($userLoginType != ADMIN && $disabled == '') {
						echo '<button type="submit" class="btn btn-primary">Complete</button>';
					}
					?>
                  </div>
                </form>
              </div>
              <!-- /.card -->
            </div>
          </div>
          <!-- /.row (main row) -->

          <div class="row">
            <div class="col-sm-12">
              <div class="team-table">

              </div>
            </div>
          </div>

        </div><!-- /.container-fluid -->
      </section>
      <!-- /.content -->
    </div>
