<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0">Dashboard</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Management Admin</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<!-- Small boxes (Stat box) -->
			<div class="row">

				<?php if (in_array($role, ['site_engineer', 'contractor', 'operation_head', 'project_head', 'project_manager', 'admin', 'area_manager'])) { ?>

					<div class="col-lg-3 col-6">
						<!-- small box -->
						<div class="small-box bg-info">
							<div class="inner">
								<h3>
									<?= $counts_data['total_projects']['count'] ?>
								</h3>

								<p>Total Projects</p>
							</div>
							<div class="icon">
								<i class="ion ion-bag"></i>
							</div>
							<a
								href = "<?= base_url($counts_data['total_projects']['url']) ?>"
								class = "small-box-footer"
							>More info <i class="fas fa-arrow-circle-right"></i></a>
						</div>
					</div>
				<?php } ?>

				<?php if (in_array($role, ['site_engineer', 'contractor', 'operation_head', 'project_head', 'project_manager', 'admin', 'area_manager'])) { ?>

					<div class="col-lg-3 col-6">
						<!-- small box -->
						<div class="small-box bg-success">
							<div class="inner">
								<h3>
									<?= $counts_data['total_open_projects']['count'] ?>
								</h3>

								<p>Open Projects</p>
							</div>
							<div class="icon">
								<i class="ion ion-stats-bars"></i>
							</div>
							<a href="<?= base_url($counts_data['total_open_projects']['url']) ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
						</div>
					</div>
				<?php } ?>


				<?php if (in_array($role, ['operation_head', 'project_head', 'project_manager', 'admin'])) { ?>
					<div class="col-lg-3 col-6">
					<!-- small box -->
					<div class="small-box bg-warning">
						<div class="inner">
							<h3>
								<?= $counts_data['users']['count'] ?>
							</h3>

							<p>User Registrations</p>
						</div>
						<div class="icon">
							<i class="ion ion-person-add"></i>
						</div>
						<a href="<?= base_url($counts_data['users']['url']) ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
						</div>
					</div>
				<?php } ?>

				<?php if (in_array($role, ['site_engineer', 'contractor', 'operation_head', 'project_head', 'project_manager', 'admin', 'area_manager'])) { ?>
					<div class="col-lg-3 col-6">
						<!-- small box -->
						<div class="small-box bg-danger">
							<div class="inner">
								<h3>
									<?= $counts_data['total_completed_projects']['count'] ?>
								</h3>

								<p>Completed Projects</p>
							</div>
							<div class="icon">
								<i class="ion ion-pie-graph"></i>
							</div>
							<a href="<?= base_url($counts_data['total_completed_projects']['url']) ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
						</div>
					</div>
				<?php } ?>

				<?php if (in_array($role, ['operation_head', 'admin'])) { ?>
					<div class="col-lg-3 col-6">
						<!-- small box -->
						<div class="small-box bg-warning">
							<div class="inner">
								<h3>
									<?= $counts_data['total_workorder']['count'] ?>
								</h3>

								<p>Total Work Order</p>
							</div>
							<div class="icon">
								<i class="ion ion-person-add"></i>
							</div>
							<a href="<?= base_url($counts_data['total_workorder']['url']) ?>" class="small-box-footer">More info <i
									class="fas fa-arrow-circle-right"></i></a>
						</div>
					</div>
				<?php } ?>

				<?php if (in_array($role, ['operation_head', 'project_head', 'project_manager', 'admin'])) { ?>
					<div class="col-lg-3 col-6">
						<!-- small box -->
						<div class="small-box bg-danger">
							<div class="inner">
								<h3>
									<?= $counts_data['today_report']['count'] ?>
								</h3>

								<p>Today Reports</p>
							</div>
							<div class="icon">
								<i class="ion ion-pie-graph"></i>
							</div>
							<a href="<?= base_url($counts_data['today_report']['url']) ?>" class="small-box-footer">More info <i
									class="fas fa-arrow-circle-right"></i></a>
						</div>
					</div>
				<?php } ?>

				<?php if (in_array($role, ['site_engineer', 'contractor', 'admin', 'area_manager'])) { ?>
					<div class="col-lg-3 col-6">
						<!-- small box -->
						<div class="small-box bg-green">
							<div class="inner">
								<h3>
									<?= $counts_data['today_update']['count'] ?>
								</h3>

								<p>Today Updates</p>
							</div>
							<div class="icon">
								<i class="ion ion-pie-graph"></i>
							</div>
							<a href="<?= base_url($counts_data['today_update']['url']) ?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
						</div>
					</div>
				<?php } ?>
			</div>
			<!-- /.row -->

		</div><!-- /.container-fluid -->
	</section>
	<!-- /.content -->
</div>
<!-- /.content-wrapper -->
