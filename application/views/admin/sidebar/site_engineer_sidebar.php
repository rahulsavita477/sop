<li class="nav-item">
	<a href="<?php echo base_url('Teams'); ?>" class="nav-link <?= $this->uri->segment('1') == 'Teams' ? 'active' : '' ?>">
		<i class="fas fa-users"></i>
	  	<p>Team</p>
	</a>
</li>

<li class="nav-item">
	<a
		href = "<?= base_url('Siteengineer/Projects') ?>"
		class = "nav-link
				<?= (
					$this->uri->segment('2') == 'Projects' &&
					$this->uri->segment('3') != '1'
				) || $this->uri->segment('4') == '2' ? 'active' : ''
				?>"
	>
		<i class="fas fa-tasks"></i>
		<p>
			Projects
		</p>
	</a>
</li>

<li class="nav-item">
	<a
		href = "<?= base_url('Siteengineer/Projects/1') ?>"
		class = "nav-link
				<?= (
					$this->uri->segment('3') == '1' &&
					$this->uri->segment('4') != '1' &&
					$this->uri->segment('4') != '2') ? 'active' : '' 
				?>"
	>
		<i class="fas fa-tasks"></i>
		<p>
			Completed Sites
		</p>
	</a>

</li>

<li class="nav-item">
	<a
		href = "<?= base_url('Siteengineer/Projects/1/1') ?>"
		class = "nav-link <?= $this->uri->segment('4') == '1' ? 'active' : '' ?>"
	>
		<i class="fas fa-users-cog"></i>
		<p>
			Completed Projects
		</p>
	</a>
</li>