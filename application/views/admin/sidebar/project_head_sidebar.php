<li class="nav-item">
	<a href="<?php echo base_url('Teams'); ?>" class="nav-link <?= $this->uri->segment('1') == 'Teams' ? 'active' : '' ?>">
		<i class="fas fa-users"></i>
	  	<p>Team</p>
	</a>
</li>

<li class="nav-item">
	<a href="<?= base_url('workorder/order_received'); ?>" class="nav-link <?= $this->uri->segment('1') == 'workorder' ? 'active' : '' ?>">
		<i class="fas fa-users-cog"></i>
		<p>Recieved Order details</p>
	</a>
</li>

<li class="nav-item">
	<a href="<?php echo base_url('Projects'); ?>" class="nav-link <?= $this->uri->segment('1') == 'Projects' ? 'active' : '' ?>">
		<i class="fas fa-tasks"></i>
		<p>Projects</p>
	</a>
</li>

<li class="nav-item">
	<a href="<?= base_url('Forms') ?>" class="nav-link <?= ($this->uri->segment('1') == 'Forms' && $this->uri->segment('2') != 'Create') ? 'active' : '' ?>">
		<i class="fas fa-tasks"></i>
	  	<p>Form Fields</p>
	</a>
</li>

<li class="nav-item">
	<a href="<?= base_url('Forms/Create') ?>" class="nav-link <?= $this->uri->segment('2') == 'Create' ? 'active' : '' ?>">
		<i class="fas fa-tasks"></i>
	  	<p>Create Form Field</p>
	</a>
</li>