<li class="nav-item">
	<a href="<?php echo base_url('Teams'); ?>" class="nav-link <?= $this->uri->segment('1') == 'Teams' ? 'active' : '' ?>">
		<i class="fas fa-users"></i>
	  	<p>Team</p>
	</a>
</li>
  
<!-- <li class="nav-item">
	<a href="<?= base_url('Projects') ?>" class="nav-link <?= $this->uri->segment('1') == 'Projects' ? 'active' : '' ?>">
		<i class="fas fa-tasks"></i>
	  	<p>Projects</p>
	</a>
</li> -->

<li class="nav-item">
	<a
		href="<?= base_url('Report') ?>"
		class="nav-link
			<?= $this->uri->segment('1') == 'Report' ||
			$this->uri->segment('1') == 'Workorder' ||
			$this->uri->segment('1') == 'Siteengineer' ||
			$this->uri->segment('1') == 'Product' ? 'active' : '' ?>"
	>
		<i class="fas fa-tasks"></i>
	  	<p>Report</p>
	</a>
</li>