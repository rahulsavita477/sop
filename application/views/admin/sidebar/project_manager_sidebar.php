<li class="nav-item">
	<a href="<?php echo base_url('Teams'); ?>" class="nav-link <?= $this->uri->segment('1') == 'Teams' ? 'active' : '' ?>">
		<i class="fas fa-users"></i>
	  	<p>Team</p>
	</a>
</li>

<li class="nav-item"> 
	<a
		href="<?= base_url('Projectmanager/projects') ?>"
		class="nav-link <?=
			$this->uri->segment('1') == 'Projectmanager'
		? 'active' : '' ?>">
		<i class="fas fa-tasks"></i>
		<p>
			My Projects
		</p>
	</a>
</li>