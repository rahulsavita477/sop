<li class="nav-item">
	<a href="<?php echo base_url('Teams'); ?>" class="nav-link <?= $this->uri->segment('1') == 'Teams' ? 'active' : '' ?>">
		<i class="fas fa-users"></i>
	  	<p>Team</p>
	</a>
</li>

<li class="nav-item">
  <a
  	href="<?= base_url('Workorder') ?>"
	class="nav-link <?= (
		$this->uri->segment('1') == 'Workorder' &&
		$this->uri->segment('2') != 'Projects' &&
		$this->uri->segment('2') != 'sites'
	) ? 'active' : '' ?>">
	<i class="fas fa-users"></i>
	<p>
	  Create work order
	</p>
  </a>
</li>

<li class="nav-item">
	<a
		href="<?= base_url('Report') ?>" 
		class="nav-link 
			<?= $this->uri->segment('1') == 'Report' ||
			$this->uri->segment('1') == 'Siteengineer' ||
			$this->uri->segment('1') == 'Product' ||
			$this->uri->segment('3') == '2'
		? 'active' : '' ?>"
	>
		<i class="fas fa-tasks"></i>
	  	<p>Report</p>
	</a>
</li>

<!-- <li class="nav-item">
  <a href="<?= base_url('Workorder/Projects') ?>" class="nav-link <?= $this->uri->segment('2') == 'Projects' || $this->uri->segment('2') == 'sites' ? 'active' : '' ?>">
	<i class="fas fa-file-alt"></i>
	<p>
	  Reports
	</p>
  </a>
</li> -->