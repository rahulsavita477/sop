 <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/'); ?>plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Product & Material</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Project Head</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        
        <!-- Main row -->
        <div class="row">
		
			<div class="col-sm-12">
				<!-- general form elements -->
            <div class="card card-primary">
              
				<!-- form start -->
              <form method="post" action="<?= base_url('Product/save_product') ?>">
            <?php
            if($this->session->flashdata('response')){
              echo $this->session->flashdata('response');
              $this->session->unset_userdata('response');
            } ?>
			    <div class="err_datasse"><?php echo validation_errors(); ?></div>
                 
                <input type="hidden" name="project_id" value="<?= $project_id ?>" />
                <input type="hidden" name="id" value="<?= isset($id) ? $id : '' ?>" />

                <div class="card-body">
			        <div class="form-group">
                        <label>Product Name</label>
                        <select name="product_name" required id="" class="form-control">
					        <option 
                                value="Solar Dual Pump" 
                                <?= isset($product_name) && $product_name == 'Solar Dual Pump' ? 'selected="selected"' : '' ?>
                            >Solar Dual Pump</option>
					        <option 
                                <?= isset($product_name) && $product_name == 'Irrigation Pump' ? 'selected="selected"' : '' ?>
                                value="Irrigation Pump">Irrigation Pump</option>
					    </select>
                    </div>
			        <div class="form-group">
                        <label>Pump Capacity</label>
                        <select name="pump_capacity" required id="" class="form-control">
					        <option 
                                <?= isset($pump_capacity) && $pump_capacity == '1 HP' ? 'selected="selected"' : '' ?>
                                value="1 HP">1 HP</option>
					        <option 
                                <?= isset($pump_capacity) && $pump_capacity == '3 HP' ? 'selected="selected"' : '' ?>
                                value="3 HP">3 HP</option>
					        <option 
                                <?= isset($pump_capacity) && $pump_capacity == '5 HP' ? 'selected="selected"' : '' ?>
                                value="5 HP">5 HP</option>
					        <option 
                                <?= isset($pump_capacity) && $pump_capacity == '7.5 HP' ? 'selected="selected"' : '' ?>
                                value="7.5 HP">7.5 HP</option>
					        <option 
                                <?= isset($pump_capacity) && $pump_capacity == '10 HP' ? 'selected="selected"' : '' ?>
                                value="10 HP">10 HP</option>
					    </select>
                    </div>
                    <div class="form-group">
                        <label>Pump Type</label>
                        <select name="pump_type" required id="" class="form-control">
					        <option 
                                <?= isset($pump_type) && $pump_type == 'Submersible' ? 'selected="selected"' : '' ?>
                                value="Submersible">Submersible</option>
					        <option 
                                <?= isset($pump_type) && $pump_type == 'Surface' ? 'selected="selected"' : '' ?>
                                value="Surface">Surface</option>
					    </select>
                    </div>
                    <div class="form-group">
                        <label>Power Type</label>
                        <select name="power_type" required id="" class="form-control">
					        <option 
                                <?= isset($power_type) && $power_type == 'DC' ? 'selected="selected"' : '' ?>
                                value="DC">DC</option>
					        <option 
                                <?= isset($power_type) && $power_type == 'AC' ? 'selected="selected"' : '' ?>
                                value="AC">AC</option>
					    </select>
                    </div>
                    <div class="form-group">
                        <label>Pump Head (Mtrs)</label>
                        <select name="pump_head" required id="" class="form-control">
					        <option 
                                <?= isset($pump_head) && $pump_head == '20' ? 'selected="selected"' : '' ?>
                                value="20">20</option>
					        <option 
                                <?= isset($pump_head) && $pump_head == '30' ? 'selected="selected"' : '' ?>
                                value="30">30</option>
					        <option 
                                <?= isset($pump_head) && $pump_head == '45' ? 'selected="selected"' : '' ?>
                                value="45">45</option>
					        <option 
                                <?= isset($pump_head) && $pump_head == '50' ? 'selected="selected"' : '' ?>
                                value="50">50</option>
					        <option 
                                <?= isset($pump_head) && $pump_head == '70' ? 'selected="selected"' : '' ?>
                                value="70">70</option>
					        <option 
                                <?= isset($pump_head) && $pump_head == '100' ? 'selected="selected"' : '' ?>
                                value="100">100</option>
					    </select>
                    </div>
                    <div class="form-group">
                        <label>Structure Height (Mtrs)</label>
                        <select name="structure_height" required id="" class="form-control">
					        <option 
                                <?= isset($structure_height) && $structure_height == '6' ? 'selected="selected"' : '' ?>
                                value="6">6</option>
					        <option 
                                <?= isset($structure_height) && $structure_height == '9' ? 'selected="selected"' : '' ?>
                                value="9">9</option>
					        <option 
                                <?= isset($structure_height) && $structure_height == '12' ? 'selected="selected"' : '' ?>
                                value="12">12</option>
					        <option 
                                <?= isset($structure_height) && $structure_height == 'NA' ? 'selected="selected"' : '' ?>
                                value="NA">NA</option>
					    </select>
                    </div>
                    <div class="form-group">
                        <label>Panel Capacity (WP)</label>
                        <select name="Panel_capacity" required id="" class="form-control">
					        <option 
                                <?= isset($Panel_capacity) && $Panel_capacity == '1200' ? 'selected="selected"' : '' ?>
                                value="1200">1200</option>
					        <option 
                                <?= isset($Panel_capacity) && $Panel_capacity == '3000' ? 'selected="selected"' : '' ?>
                                value="3000">3000</option>
					        <option 
                                <?= isset($Panel_capacity) && $Panel_capacity == '4800' ? 'selected="selected"' : '' ?>
                                value="4800">4800</option>
					        <option 
                                <?= isset($Panel_capacity) && $Panel_capacity == '6750' ? 'selected="selected"' : '' ?>
                                value="6750">6750</option>
					        <option 
                                <?= isset($Panel_capacity) && $Panel_capacity == '9000' ? 'selected="selected"' : '' ?>
                                value="9000">9000</option>
					    </select>
                    </div>
                    
                    <div class="form-group">
                        <label>Structure Staging</label>
                        <select name="structure_staging" required id="" class="form-control">
					        <option 
                                <?= isset($structure_staging) && $structure_staging == 'Double' ? 'selected="selected"' : '' ?>
                                value="Double">Double</option>
					        <option 
                                <?= isset($structure_staging) && $structure_staging == 'Single' ? 'selected="selected"' : '' ?>
                                value="Single">Single</option>
					        <option 
                                <?= isset($structure_staging) && $structure_staging == 'NA' ? 'selected="selected"' : '' ?>
                                value="NA">NA</option>
					    </select>
                    </div>
                    
                    <div class="form-group">
                        <label>Water Tank Capacity (Ltrs)</label>
                        <select name="water_tank_capacity" required id="" class="form-control">
					        <option 
                                <?= isset($water_tank_capacity) && $water_tank_capacity == '10000' ? 'selected="selected"' : '' ?>
                                value="10000">10000</option>
					        <option 
                                <?= isset($water_tank_capacity) && $water_tank_capacity == 'NA' ? 'selected="selected"' : '' ?>
                                value="NA">NA</option>
					        <option 
                                <?= isset($water_tank_capacity) && $water_tank_capacity == '5000' ? 'selected="selected"' : '' ?>
                                value="5000">5000</option>
					    </select>
                    </div>
                    <br/>
                    <div class="form-group">
                        <label>Civil Material to be Supplied Details</label>
                    </div>
                    
                    <div class="form-group">
                        <label>Foundation Bolts (No.s)</label>
                        <select name="foundation_bolt" required id="" class="form-control">
					        <option 
                                <?= isset($foundation_bolt) && $foundation_bolt == '4' ? 'selected="selected"' : '' ?>
                                value="4">4</option>
					        <option 
                                <?= isset($foundation_bolt) && $foundation_bolt == '8' ? 'selected="selected"' : '' ?>
                                value="8">8</option>
					        <option 
                                <?= isset($foundation_bolt) && $foundation_bolt == '16' ? 'selected="selected"' : '' ?>
                                value="16">16</option>
					        <option 
                                <?= isset($foundation_bolt) && $foundation_bolt == '24' ? 'selected="selected"' : '' ?>
                                value="24">24</option>
					        <option 
                                <?= isset($foundation_bolt) && $foundation_bolt == '36' ? 'selected="selected"' : '' ?>
                                value="36">36</option>
					        <option 
                                <?= isset($foundation_bolt) && $foundation_bolt == 'NA' ? 'selected="selected"' : '' ?>
                                value="NA">NA</option>
					    </select>
                    </div>
                    
                    <div class="form-group">
                        <label>Civil poles (No.s)</label>
                        <select name="civil_poles" required id="" class="form-control">
					        <option 
                                <?= isset($civil_poles) && $civil_poles == '1' ? 'selected="selected"' : '' ?>
                                value="1">1</option>
					        <option 
                                <?= isset($civil_poles) && $civil_poles == '2' ? 'selected="selected"' : '' ?>
                                value="2">2</option>
					        <option 
                                <?= isset($civil_poles) && $civil_poles == '3' ? 'selected="selected"' : '' ?>
                                value="3">3</option>
					        <option 
                                <?= isset($civil_poles) && $civil_poles == '4' ? 'selected="selected"' : '' ?>
                                value="4">NA</option>
					    </select>
                    </div>
                    <div class="form-group">
                        <label>Cement (In Bags)</label>
                        <select name="cement" required id="" class="form-control">
					        <option 
                                <?= isset($cement) && $cement == '58' ? 'selected="selected"' : '' ?>
                                value="58">58</option>
					        <option 
                                <?= isset($cement) && $cement == '100' ? 'selected="selected"' : '' ?>
                                value="100">100</option>
					        <option 
                                <?= isset($cement) && $cement == 'NA' ? 'selected="selected"' : '' ?>
                                value="NA">NA</option>
					    </select>
                    </div>
                    
                    <div class="form-group">
                        <label>TMT (No.of Bars)	8 MM</label>
                        <select name="tmt_8mm" required id="" class="form-control">
					        <option 
                                <?= isset($tmt_8mm) && $tmt_8mm == '13' ? 'selected="selected"' : '' ?>
                                value="13">13</option>
					        <option 
                                <?= isset($tmt_8mm) && $tmt_8mm == '20' ? 'selected="selected"' : '' ?>
                                value="20">20</option>
					        <option 
                                <?= isset($tmt_8mm) && $tmt_8mm == 'NA' ? 'selected="selected"' : '' ?>
                                value="NA">NA</option>
					    </select>
                    </div>
                    
                    <div class="form-group">
                        <label>TMT (No.of Bars)	10 MM</label>
                        <select name="tmt_10mm" required id="" class="form-control">
					        <option 
                                <?= isset($tmt_10mm) && $tmt_10mm == '43' ? 'selected="selected"' : '' ?>
                                value="43">43</option>
					        <option 
                                <?= isset($tmt_10mm) && $tmt_10mm == '84' ? 'selected="selected"' : '' ?>
                                value="84">84</option>
					        <option 
                                <?= isset($tmt_10mm) && $tmt_10mm == 'NA' ? 'selected="selected"' : '' ?>
                                value="NA">NA</option>
					    </select>
                    </div>
                    
                    <div class="form-group">
                        <label>TMT (No.of Bars)	12 MM</label>
                        <select name="tmt_12mm" required id="" class="form-control">
					        <option 
                                <?= isset($tmt_12mm) && $tmt_12mm == '10' ? 'selected="selected"' : '' ?>
                                value="10">10</option>
					        <option 
                                <?= isset($tmt_12mm) && $tmt_12mm == '15.5' ? 'selected="selected"' : '' ?>
                                value="15.5">15.5</option>
					        <option 
                                <?= isset($tmt_12mm) && $tmt_12mm == 'NA' ? 'selected="selected"' : '' ?>
                                value="NA">NA</option>
					    </select>
                    </div>
                    <br/>
                    <div class="form-group">
                        <label>Instalation Material To be SuppliedDetaila</label>
                    </div>
                    <div class="form-group">
                        <label>Structure  Material (In Set)</label>
                        <select name="structure_material" required id="" class="form-control">
					        <option 
                                <?= isset($structure_material) && $structure_material == '1' ? 'selected="selected"' : '' ?>
                                value="1">1</option>
					        <option 
                                <?= isset($structure_material) && $structure_material == '2' ? 'selected="selected"' : '' ?>
                                value="2">2</option>
					        <option 
                                <?= isset($structure_material) && $structure_material == '3' ? 'selected="selected"' : '' ?>
                                value="3">3</option>
					    </select>
                    </div>
                    
                    <div class="form-group">
                        <label>BOS Material (In Set)</label>
                        <select name="bos_material" required id="" class="form-control">
					        <option 
                                <?= isset($bos_material) && $bos_material == 'NA' ? 'selected="selected"' : '' ?>
                                value="NA">NA</option>
					        <option 
                                <?= isset($bos_material) && $bos_material == '1' ? 'selected="selected"' : '' ?>
                                value="1">1</option>
					        <option 
                                <?= isset($bos_material) && $bos_material == '2' ? 'selected="selected"' : '' ?>
                                value="2">2</option>
					    </select>
                    </div>
                    
                    <div class="form-group">
                        <label>Sumbersible Pump (No.s)</label>
                        <select name="sumbersible_pump" required id="" class="form-control">
					        <option 
                                <?= isset($sumbersible_pump) && $sumbersible_pump == '1' ? 'selected="selected"' : '' ?>
                                value="1">1</option>
					    </select>
                    </div>
                    
                    <div class="form-group">
                        <label>Controller (No.s)</label>
                        <select name="controllers" required id="" class="form-control">
					        <option 
                                <?= isset($controllers) && $controllers == '1' ? 'selected="selected"' : '' ?>
                                value="1">1</option>
					    </select>
                    </div>
                    
                    <div class="form-group">
                        <label>Water Tank</label>
                        <select name="water_tank" required id="" class="form-control">
					        <option 
                                <?= isset($water_tank) && $water_tank == '1' ? 'selected="selected"' : '' ?>
                                value="1">1</option>
					        <option 
                                <?= isset($water_tank) && $water_tank == '2' ? 'selected="selected"' : '' ?>
                                value="2">2</option>
					        <option 
                                <?= isset($water_tank) && $water_tank == '3' ? 'selected="selected"' : '' ?>
                                value="NA">NA</option>
					    </select>
                    </div>
                    
                    <div class="form-group">
                        <label>Solar Panel</label>
                        <select name="solar_panel" required id="" class="form-control">
					        <option 
                                <?= isset($solar_panel) && $solar_panel == '4' ? 'selected="selected"' : '' ?>
                                value="4">4</option>
					        <option 
                                <?= isset($solar_panel) && $solar_panel == '9' ? 'selected="selected"' : '' ?>
                                value="9">9</option>
					        <option 
                                <?= isset($solar_panel) && $solar_panel == '10' ? 'selected="selected"' : '' ?>
                                value="10">10</option>
					        <option 
                                <?= isset($solar_panel) && $solar_panel == '14' ? 'selected="selected"' : '' ?>
                                value="14">14</option>
					        <option 
                                <?= isset($solar_panel) && $solar_panel == '15' ? 'selected="selected"' : '' ?>
                                value="15">15</option>
					        <option 
                                <?= isset($solar_panel) && $solar_panel == '16' ? 'selected="selected"' : '' ?>
                                value="16">16</option>
					        <option 
                                <?= isset($solar_panel) && $solar_panel == '30' ? 'selected="selected"' : '' ?>
                                value="30">30</option>
					    </select>
                    </div>
                    
                    <div class="form-group">
                        <label>Hdpe Pipe Length (Mtrs)</label>
                        <select name="hdpe_pipe_length" required id="" class="form-control">
					        <option 
                                <?= isset($hdpe_pipe_length) && $hdpe_pipe_length == '50' ? 'selected="selected"' : '' ?>
                                value="50">50</option>
					        <option 
                                <?= isset($hdpe_pipe_length) && $hdpe_pipe_length == '20' ? 'selected="selected"' : '' ?>
                                value="20">20</option>
					        <option 
                                <?= isset($hdpe_pipe_length) && $hdpe_pipe_length == 'As Per SSF' ? 'selected="selected"' : '' ?>
                                value="As Per SSF">As Per SSF</option>
					    </select>
                    </div>
                    
                    <div class="form-group">
                        <label>Wire Rope Length (Mtrs)</label>
                        <select name="wire_rope_length"required id="" class="form-control">
					        <option 
                                <?= isset($wire_rope_length) && $wire_rope_length == '50' ? 'selected="selected"' : '' ?>
                                value="50">50</option>
					        <option 
                                <?= isset($wire_rope_length) && $wire_rope_length == '20' ? 'selected="selected"' : '' ?>
                                value="20">20</option>
					        <option 
                                <?= isset($wire_rope_length) && $wire_rope_length == 'As Per SSF' ? 'selected="selected"' : '' ?>
                                value="As Per SSF">As Per SSF</option>
					    </select>
                    </div>
                    
                    <div class="form-group">
                        <label>Power Cable Length (Mtrs)</label>
                        <select name="power_cable_length"required id="" class="form-control">
					        <option 
                                <?= isset($power_cable_length) && $power_cable_length == '65' ? 'selected="selected"' : '' ?>
                                value="65">65</option>
					        <option 
                                <?= isset($power_cable_length) && $power_cable_length == '50' ? 'selected="selected"' : '' ?>
                                value="50">50</option>
					        <option 
                                <?= isset($power_cable_length) && $power_cable_length == 'As Per SSF' ? 'selected="selected"' : '' ?>
                                value="As Per SSF">As Per SSF</option>
					    </select>
                    </div>
				  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->
			</div>
			
          
        </div>
        <!-- /.row (main row) -->
		
		<div class="row">
			<div class="col-sm-12">
				<div class="team-table">
					
				</div>
			</div>
		</div>
		
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>