<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Common_models extends CI_Model {
    
    public function __construct() {
    
        parent::__construct();
        
        $this->userlogin_type = $this->session->userdata('ses_userlogin_type');
    	$this->user_id = $this->session->userdata('ses_userlogin_id');
    }

    public function add_entry($table,$data) {
    
        $this->db->insert($table, $data);
    
        return $this->db->insert_id();
    }

    public function get_entries() {

        $query = $this->db->get('employee');
        
        return $query->result();
    }

    public function update_entry($table, $data, $where) {
    
        $this->db->where($where);
        $qyer = $this->db->update("$table", $data);
	
        return $qyer;
    }

	public function counts_data($table, $where=null, $select=null, $where_in=null) {
		
        if($select)
		{
			$this->db->select($select); 
		}

        if($where)
        {
           $this->db->where($where); 
        }
        
        if ($where_in) {
            $this->db->where_in($where_in[0], $where_in[1]);
        }

        $query = $this->db->get($table);
        
		return $query->num_rows();
    }

	public function counts_data_total_req($sql) {
		$count_r=0;
		$row=$this->db->query($sql)->row_array();
		if($row['totalcount'])
		{
			$count_r=$row['totalcount'];
		}
		return $count_r;
    }

    public function get_entry($table, $where=null, $orderby=null, $order=null, $limit=null, $offset=0, $select=null, $where_in=null) {
        
        if($where) {
           $this->db->where($where);
        }
        
        if($orderby) {
            $this->db->order_by($orderby, $order);
        }
        else {
            $this->db->order_by('update_date', 'DESC');
        }

        if($limit) {
            $this->db->limit($limit,$offset);
        }
        
        if ($select) {
            $this->db->select($select);
        }

        if ($where_in) {
            $this->db->where_in($where_in[0], $where_in[1]);
        }

        $query = $this->db->get($table);
        
        if($query->num_rows()) {
			return $query->result_array();
		} else {
			return array();
		}
    }

	public function get_entry_row($table,$where=null,$orderby=null,$order=null,$limit=null) {
        
        if($where) {
           $this->db->where($where);
        }
        
        if($orderby) {
            $this->db->order_by($orderby, $order);
        }

        if($limit) {
            $this->db->limit($limit);
        }
        
        $query = $this->db->get($table);
		if($query->num_rows()) {
			return $query->row_array();
		} else {
			return false;
		}
    }

    public function delete_entry($table, $where) {
        return $this->db->delete($table, $where);
    }

	public function generateRandomString($length=4) {

		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';

		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		
        return $randomString;
	}
    
	public function password_creator($password) {

		$pass_rand = $this->Common_models->generateRandomString();
		$new_password = md5(md5($password).$pass_rand);
		
        return array('password' => $new_password, 'SALT' => $pass_rand);
	}
	
    public function getProjectIds() {

        $finalOutput = [];

        // get project list
        $where = null;
        $where_in = null;
        
        if($this->userlogin_type == 'project_manager') {
        
            $where['FIND_IN_SET('.$this->user_id.', project_manager)'] = null;
            $where['assigned'] = 1;
            
        } else if($this->userlogin_type == 'operation_head') {

            // get loggedin operation head workorder id 
            $operation_head_prjs = $this->get_entry("workorder_tbl", ['created_by' => $this->user_id], null, null, null, 0, 'id AS woId');
            $where_in = ['workorder_id', array_column($operation_head_prjs, 'woId')];
        }

		$projects = $this->get_entry("project_tbl", $where, null, null, null, 0, 'id AS prjId', $where_in);
        
        if(count($projects) > 0) {

            if($this->userlogin_type == 'site_engineer') $where1['site_engineer'] = $this->user_id;
            else if($this->userlogin_type == 'area_manager') $where1['area_manager'] = $this->user_id;
            else if($this->userlogin_type == 'contractor') $where1['contractor'] = $this->user_id;

            foreach ($projects as $key => $value) {

				// get total project sites
				$where1['project_id'] = $value['prjId'];
                
                $totalProjectSites = $this->Common_models->get_entry('sites_tbl', $where1, null, null, null, 0, 'id');
				$projectSitesID = array_column($totalProjectSites, 'id');
				$totalProjectSites = count($projectSitesID);
				// echo "<pre>"; print_r($projects); die;

				// get completed sites
				if (count($projectSitesID)>0) {
                    
                    if($this->userlogin_type == 'project_manager') {

                        // get assigned sites
                        $where = [];
                        $where['project_id'] = $value['prjId'];
                        $where['assigned'] = 1;
                        $totalAssignedSites = $this->Common_models->counts_data('sites_tbl', $where);
                        $totalExecutedProjects['completedSites'] = $totalAssignedSites;
                        
                    } else {

                        $sel = "SELECT count(id) AS completedSites
							FROM contractor_execution 
							WHERE site_id IN (".implode(',', $projectSitesID).")";
                    
                        if($this->userlogin_type == 'site_engineer') {
                            $sel .= ' AND site_engineer_id = '.$this->user_id; 
                        }

                        // echo $sel; die;
                        $q = $this->db->query($sel);
                        $totalExecutedProjects = $q->row_array();
                    }					

				} else {

					$totalExecutedProjects['completedSites'] = 0;
				}			

				if ($totalProjectSites != 0 && $totalProjectSites == $totalExecutedProjects['completedSites']) {
                    $finalOutput['completeProject'][$key]['prjId'] = $value['prjId'];
				} else {
                    $finalOutput['openProject'][$key]['prjId'] = $value['prjId'];
                }
			}
        }
        
        return $finalOutput;
    }
}
?>