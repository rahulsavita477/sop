<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Projects extends MY_Controller {

	function __construct() {
        parent::__construct();
		$this->userlogin_type = $this->session->userdata('ses_userlogin_type');
    	$this->user_id=$this->session->userdata('ses_userlogin_id');
    }
	
	public function index() {

		if($this->userlogin_type == 'project_manager') {

			$sel = "SELECT project_tbl.*, workorder_tbl.order_deadline 
					FROM project_tbl 
					LEFT JOIN workorder_tbl 
						ON workorder_tbl.id = workorder_id 
					WHERE 
						assigned = 1 AND 
						FIND_IN_SET($this->user_id, project_manager) 
					ORDER BY update_date DESC";
			$q=$this->db->query($sel);
			$res=$q->result_array();
			$data['project_list']=$res;

		} else if($this->userlogin_type == 'admin') {

			// get project list
			$sql = "SELECT prj.*, wo.order_deadline 
					FROM project_tbl AS prj
					LEFT JOIN workorder_tbl AS wo 
						ON workorder_id = wo.id
					ORDER BY prj.update_date DESC";
			$q = $this->db->query($sql);
			$data['project_list'] = $q->result_array();

		} else {
			
			// get project list
			$sql = "SELECT prj.*, wo.order_deadline 
					FROM project_tbl AS prj
					LEFT JOIN workorder_tbl AS wo 
						ON workorder_id = wo.id
					WHERE prj.created_by = ".$this->user_id;
			
			if (isset($_GET['filter']) && $_GET['filter'] == 'todayUpdate') {

				$sql .= ' AND DATE(prj.update_date) = CURDATE()';

			} else if (isset($_GET['filter']) && $_GET['filter'] == 'openProject') {

				$res = $this->Common_models->getProjectIds();
				if(isset($res['openProject'])) {
					$sql .= ' AND prj.id IN ('.implode(',', array_column($res['openProject'], 'prjId')).')';
				} else {
					$sql .= ' AND prj.id = null';
				}
				
			} else if (isset($_GET['filter']) && $_GET['filter'] == 'completeProject') {
				
				$res = $this->Common_models->getProjectIds();
				if(isset($res['completeProject'])) {
					$sql .= ' AND prj.id IN ('.implode(',', array_column($res['completeProject'], 'prjId')).')';
				} else {
					$sql .= ' AND prj.id = null';
				}
			}

			$sql .= " ORDER BY prj.update_date DESC";
			$q = $this->db->query($sql);
			$data['project_list'] = $q->result_array();

			// echo "<pre>"; print_r($data); die;
		}

		$where1 = array(
			'position_type='=>'project_manager',
			'status'=>1
		);
		$data['project_managers'] = $this->Common_models->get_entry('admin_tbl',$where1);

		$this->load->view('admin/common/header');
		$this->load->view('admin/projectlist_view', $data);
		$this->load->view('admin/common/footer');
	}

	public function create($workorder_id)
	{
		$where=array(
			'position_type='=>'project_manager',
			'status'=>1
		);
		$data['project_managers'] = $this->Common_models->get_entry('admin_tbl',$where);
		$data['statelists'] = $this->Common_models->get_entry('states','','name','ASC');
		$data['workorder_id'] = $workorder_id;

		// get project details
		$data['prj_details'] = $this->Common_models->get_entry_row('project_tbl', ['workorder_id' => $workorder_id, 'created_by' => $this->user_id]);

		// get form all fields
		$data['execution_fields'] = $this->Common_models->get_entry('form_fields', ['form' => 'EXECUTION'], 'title', 'ASC', null, 0, 'id, title');
		$data['gathering_fields'] = $this->Common_models->get_entry('form_fields', ['form' => 'GATHERING'], 'title', 'ASC', null, 0, 'id, title');
		$data['icr_fields'] = $this->Common_models->get_entry('form_fields', ['form' => 'ICR'], 'title', 'ASC', null, 0, 'id, title');
		$data['survey_fields'] = $this->Common_models->get_entry('form_fields', ['form' => 'SURVEY'], 'title', 'ASC', null, 0, 'id, title');

		// echo "<pre>"; print_r($data); die;

		$this->load->view('admin/common/header');
		$this->load->view('admin/create_project_view', $data);
		$this->load->view('admin/common/footer');
	}
	
	public function add_project()
	{
		$postdata=$this->input->post();
		$this->form_validation->set_rules('state', 'State', 'trim|required');
		$this->form_validation->set_rules('project_name', 'Project Name', 'trim|required');
		// $this->form_validation->set_rules('project_manager[]', 'Project Manager', 'trim|required');
		$this->form_validation->set_rules('define_phase', 'Phase', 'trim|required');
		if($this->form_validation->run() == FALSE)
        {
			$where=array(
			'position_type='=>'project_manager',
			'status'=>1
			);
			$data['project_managers']=$this->Common_models->get_entry('admin_tbl', $where);
			$data['statelists']=$this->Common_models->get_entry('states','','name','ASC');
			$this->load->view('admin/common/header');
			$this->load->view('admin/create_project_view',$data);
			$this->load->view('admin/common/footer');
		}
		else 
		{
			$project_manager='';
			// if($postdata['project_manager'])
			// {
			// 	$project_manager=implode(",",$postdata['project_manager']);
			// }

			$user_id=$this->session->userdata('ses_userlogin_id');
			$insertdata['state']=$postdata['state'];
			$insertdata['project_name']=$postdata['project_name'];
			$insertdata['project_manager']=$project_manager;
			$insertdata['define_phase']=$postdata['define_phase'];
			$insertdata['scope_deadlines']=$postdata['work_deadlines'];
			$insertdata['workorder_id'] = $postdata['workorder_id'];
			$insertdata['created_by']=$user_id;
			$insertdata['create_date']=date('Y-m-d H:i:s');

			if(!empty($_FILES['site_file']['name']))
			{
				$ext=explode(".",$_FILES['site_file']['name']);
				$ext1=end($ext);
				$file_name=rand(22,9999).time().".".$ext1;
				if(move_uploaded_file($_FILES['site_file']['tmp_name'],"assets/project_document/$file_name"))
				{
					$insertdata['site_file']=$file_name;
				}
			}

			$insertdata['survey_file'] = '';
			/*if(!empty($_FILES['survey_file']['name']))
			{
				$ext=explode(".",$_FILES['survey_file']['name']);
				$ext1=end($ext);
				$file_name=rand(22,9999).time().".".$ext1;
				if(move_uploaded_file($_FILES['survey_file']['tmp_name'],"assets/project_document/$file_name"))
				{
					$insertdata['survey_file']=$file_name;
				}
			}*/

			if(!empty($_FILES['icr_file']['name']))
			{
				$ext=explode(".",$_FILES['icr_file']['name']);
				$ext1=end($ext);
				$file_name=rand(22,9999).time().".".$ext1;
				if(move_uploaded_file($_FILES['icr_file']['tmp_name'],"assets/project_document/$file_name"))
				{
					$insertdata['icr_file']=$file_name;
				}
			}

			// dynamic form fields 
			$insertdata['survey_form_fields'] = json_encode($postdata['survey_form_fields']);
			$insertdata['ICR_form_fields'] = json_encode($postdata['ICR_form_fields']);
			$insertdata['gathering_form_fields'] = json_encode($postdata['gathering_form_fields']);
			$insertdata['execution_form_fields'] = json_encode($postdata['execution_form_fields']);

			// Check whether need to update or add 
			$cnt=$this->Common_models->counts_data('project_tbl', ['created_by' => $user_id, 'workorder_id' => $postdata['workorder_id']]);
			if ($cnt==0) {
				$add_data=$this->Common_models->add_entry('project_tbl', $insertdata);
			} else {
				$add_data=$this->Common_models->update_entry('project_tbl', $insertdata, ['created_by' => $user_id, 'workorder_id' => $postdata['workorder_id']]);
			}

			if($add_data)
			{
				if(!empty($insertdata['site_file']))
				{
					$row = 1;
					if (($handle = fopen("assets/project_document/".$insertdata['site_file'], "r")) !== FALSE) {
					  while(($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
						$num = count($data);
						if($row==1 || empty($data[0]) || $data[0] == '')
						{
							$row++;
							continue;
						}
						$row++;
						$array_site=array();
						$array_site['circle_name']=$data[0];
						$array_site['land_district']=$data[1];
						$array_site['land_village']=$data[2];
						$array_site['land_taluka']=$data[3];
						$array_site['workorder_no']=$data[4];
						$array_site['beneficiary_id']=$data[5];
						$array_site['beneficiary_name']=$data[6];
						$array_site['mobilen_number']=$data[7];
						$array_site['land_address']=$data[8];
						$array_site['pump_load']=$data[9];
						$array_site['category']=$data[10];
						$array_site['work_order_date']=date('Y-m-d H:i:s',strtotime($data[11]));
						$array_site['application_status']=$data[12];
						$array_site['installation_status']=$data[13];
						$array_site['installation_date']=date('Y-m-d',strtotime($data[14]));
						$array_site['remarks']=$data[15];
						$array_site['lot']=$data[16];
						$array_site['create_date']=date('Y-m-d H:i:s');
						$array_site['project_id']=$add_data;
						$array_site['added_by']=$user_id;
						$this->Common_models->add_entry('sites_tbl',$array_site);
					  }
					  fclose($handle);
					}
				}
			}

			$this->session->set_flashdata('response','<p class="alert alert-success">Success! Project added successfully.</p>');
			
			// return redirect('Projects/create/'.$postdata['workorder_id']);
			return redirect('workorder/order_received');
		}
	}
	
	public function assign_manager($project_id)
	{
		$user_id=$this->session->userdata('ses_userlogin_id');
		$postdata=$this->input->post();
		$this->form_validation->set_rules('assign_project_manager[]', 'Project Manager', 'trim|required');
		if($this->form_validation->run() == FALSE)
        {
			$this->session->set_flashdata('response','<p class="alert alert-danger">Error! Please select a Project managers.</p>');
		}
		else 
		{
			$project_manager='';
			if($postdata['assign_project_manager'])
			{
				$project_manager=implode(",",$postdata['assign_project_manager']);
			}
			$updatedata['project_manager']=$project_manager;
			$updatedata['assigned']=1;
			$updatedata['assigned_by']=$user_id;
			$updatedata['assigned_at']=date('Y-m-d H:i:s');
			$where['id']=$project_id;
			$update=$this->Common_models->update_entry('project_tbl',$updatedata,$where);
			$this->session->set_flashdata('response','<p class="alert alert-success">Success! Project assigned successfully.</p>');
			
		}
		return redirect('Projects');
	}
}
?>