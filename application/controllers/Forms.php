<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Forms extends MY_Controller {

	function __construct() {

        parent::__construct();

		$this->userlogin_type = $this->session->userdata('ses_userlogin_type');
    	$this->user_id = $this->session->userdata('ses_userlogin_id');
    }

	public function index() {

		$data['forms'] = $this->Common_models->get_entry('form_fields');
		// echo "<pre>"; print_r($data); die;

		$this->load->view('admin/common/header');
		$this->load->view('admin/Form/dashboard', $data);
		$this->load->view('admin/common/footer');
	}

	public function edit($form_id) {

		$data['form'] = $this->Common_models->get_entry_row('form_fields', ['id' => $form_id]);
		
		$data['field_type_enum'] = $this->getFormENUMFields('type');

		$data['form_type_enum'] = $this->getFormENUMFields('form');

		// echo "<pre>"; print_r($data); die;

		$this->load->view('admin/common/header');
		$this->load->view('admin/Form/edit', $data);
		$this->load->view('admin/common/footer');
	}	

	public function create() {

		$data['field_type_enum'] = $this->getFormENUMFields('type');

		$data['form_type_enum'] = $this->getFormENUMFields('form');

		// echo "<pre>"; print_r($data); die;

		$this->load->view('admin/common/header');
		$this->load->view('admin/Form/create', $data);
		$this->load->view('admin/common/footer');
	}	

	public function add_form() {

		$postdata = $this->input->post();

		$data['title'] = $postdata['title'];
		$data['name'] = $postdata['name'];
		$data['type'] = $postdata['type'];
		$data['min'] = empty($postdata['min']) ? null : $postdata['min'];
		$data['max'] = empty($postdata['max']) ? null : $postdata['max'];
		$data['field_order'] = $postdata['field_order'];
		$data['form'] = $postdata['form'];
		$data['created_by'] = $this->user_id;

		if (isset($postdata['id'])) {

			$this->Common_models->update_entry('form_fields', $data, ['id' => $postdata['id']]);

			$this->session->set_flashdata('response','<p class="alert alert-success">form fields updated successfully.</p>');

		} else {

			$this->Common_models->add_entry('form_fields', $data);

			$this->session->set_flashdata('response','<p class="alert alert-success">Sites Equally assign to all.</p>');

		}

		return redirect('Forms');
	}

	private function getFormENUMFields($column) {
		$type = $this->db->query("SHOW COLUMNS FROM form_fields WHERE Field = '{$column}'")->row(0)->Type;
	    preg_match("/^enum\(\'(.*)\'\)$/", $type, $matches);
	    $enum = explode("','", $matches[1]);
	    return $enum;
	}
}