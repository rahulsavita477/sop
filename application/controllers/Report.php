<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends MY_Controller {

	function __construct() {

        parent::__construct();

		$this->userlogin_type = $this->session->userdata('ses_userlogin_type');
    	$this->user_id = $this->session->userdata('ses_userlogin_id');
    }
	
	public function index() {

		if($this->userlogin_type == 'admin' || $this->userlogin_type == 'operation_head') {

			// get project list
			$sql = "SELECT prj.*, wo.order_deadline 
					FROM project_tbl AS prj
					INNER JOIN workorder_tbl AS wo 
						ON workorder_id = wo.id";

			if($this->session->userdata('ses_userlogin_type') == 'operation_head') {
				$sql .= ' AND wo.created_by = '.$this->user_id;
			}

			if (isset($_GET['filter']) && $_GET['filter'] == 'todayUpdate') {

				$sql .= ' WHERE DATE(prj.update_date) = CURDATE()';

			} else if (isset($_GET['filter']) && $_GET['filter'] == 'openProject') {

				$res = $this->Common_models->getProjectIds();
				if(isset($res['openProject'])) {
					$sql .= ' WHERE prj.id IN ('.implode(',', array_column($res['openProject'], 'prjId')).')';
				} else {
					$sql .= ' WHERE prj.id = null';
				}
				
			} else if (isset($_GET['filter']) && $_GET['filter'] == 'completeProject') {
				
				$res = $this->Common_models->getProjectIds();
				if(isset($res['completeProject'])) {
					$sql .= ' WHERE prj.id IN ('.implode(',', array_column($res['completeProject'], 'prjId')).')';
				} else {
					$sql .= ' WHERE prj.id = null';
				}
			}

            $sql .= " GROUP BY prj.id ORDER BY prj.update_date DESC";
			// echo $sql; die;
			$q = $this->db->query($sql);
			$data['project_list'] = $q->result_array();

		}

		$this->load->view('admin/common/header');
		$this->load->view('admin/Report/Admin', $data);
		$this->load->view('admin/common/footer');
	}
}
?>