<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teams extends MY_Controller {

	function __construct() {
        
        parent::__construct();
		
		$this->userlogin_type = $this->session->userdata('ses_userlogin_type');
    	$this->user_id = $this->session->userdata('ses_userlogin_id');
		
		$this->load->library('email');
    }

	public function index() {

		$where = [
			'position_type != ' => 'admin',
			'id != ' => $this->user_id
		];
		$where_in = ['position_type', array_keys(accessLavels($this->userlogin_type))];
		$data['userslist'] = $this->Common_models->get_entry('admin_tbl', $where, null, null, null, 0, null, $where_in);
		
		$this->load->view('admin/common/header');
		$this->load->view('admin/teams_view',$data);
		$this->load->view('admin/common/footer');
	}

	public function add_team() {
		
		$postdata = $this->input->post();

		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[admin_tbl.email]');
		$this->form_validation->set_rules('position_type', 'Position', 'trim|required');
		$this->form_validation->set_rules('mobile', 'Mobile Number', 'trim|required|regex_match[/^[0-9]{10}$/]');

		if(!$this->form_validation->run()) {

			$where = ['position_type!=' => 'admin'];
			$data['userslist'] = $this->Common_models->get_entry('admin_tbl', $where);

			$this->load->view('admin/common/header');
			$this->load->view('admin/teams_view',$data);
			$this->load->view('admin/common/footer');
		
		} else {

			$d = explode("@",$postdata['email']);
			$uid = $d[0];
			$password = $this->Common_models->generateRandomString(8);
			$inserdata['name'] = $postdata['name'];
			$inserdata['email'] = $postdata['email'];
			$inserdata['mobile'] = $postdata['mobile'];
			$inserdata['position_type'] = $postdata['position_type'];
			$inserdata['create_date'] = date('Y-m-d H:i:s');
			$inserdata['password'] = $password;

			$add_data = $this->Common_models->add_entry('admin_tbl', $inserdata);
			if($add_data) {

				$uid = $uid.$add_data;
				$updata['user_id'] = $uid;
				$this->Common_models->update_entry('admin_tbl', $updata, ['id' => $add_data]);
				
				$msg = 'Your login credentials are below.<br /><br />
						Username: '.$postdata['email'].'<br />
						Password: '.$password;
				sendEmail($postdata['email'], 'Credentials', $msg);
			}

			$this->session->set_flashdata('response', '<p class="alert alert-success">Success! Team added successfully.</p>');

			return redirect('Teams');
		}
	}

	public function deactivate_team($user_id)
	{
		$updata['status']=0;
		$this->Common_models->update_entry('admin_tbl',$updata,array('id'=>$user_id));
		$this->session->set_flashdata('response','<p class="alert alert-success">Success! Account deactived.</p>');
		return redirect('Teams');
	}

	public function activate_team($user_id) {
		
		$updata['status']=1;

		$this->Common_models->update_entry('admin_tbl',$updata,array('id'=>$user_id));
		$this->session->set_flashdata('response','<p class="alert alert-success">Success! Account activated.</p>');

		return redirect('Teams');
	}

	public function edit($user_id) {
		
		$data['userslist'] = $this->Common_models->get_entry('admin_tbl', ['id' => $user_id]);

		switch ($data['userslist'][0]['position_type']) {

			case OPERATION_HEAD:
				$position_type = OPERATION_HEAD;
				break;

			case PROJECT_HEAD:
				$position_type = PROJECT_HEAD;
				break;

			case PROJECT_MANAGER:
				$position_type = PROJECT_MANAGER;
				break;

			case SITE_ENGINEER:
				$position_type = SITE_ENGINEER;
				break;

			case AREA_MANAGER:
				$position_type = AREA_MANAGER;
				break;

			case CONTRACTOR:
				$position_type = CONTRACTOR;
				break;
			
			case ACCOUNT_HEAD:
				$position_type = ACCOUNT_HEAD;
				break;

			case LOGISTIC_HEAD:
				$position_type = LOGISTIC_HEAD;
				break;

			
			default: break;
		}
		
		$data['newAssignee'] = $this->Common_models->get_entry('admin_tbl', ['position_type' => $position_type]);
		
		$this->load->view('admin/common/header');
		$this->load->view('admin/edit_user', $data);
		$this->load->view('admin/common/footer');
	}

	public function update_user() {
		
		$postdata = $this->input->post();

		$this->form_validation->set_rules('position_type', 'Position', 'trim|required');

		$update = $this->Common_models->update_entry(
			'admin_tbl',
			['position_type' => $postdata['position_type']],
			['id' => $postdata['id']]
		);

		if($update) {

			// update new assignee
			if($postdata['old_role'] == SITE_ENGINEER) {
				
				$this->Common_models->update_entry(
					'contractor_execution',
					['site_engineer_id' => $postdata['new_assignee']],
					['site_engineer_id' => $postdata['id']]
				);

				$this->Common_models->update_entry(
					'product',
					['site_eng_id' => $postdata['new_assignee']],
					['site_eng_id' => $postdata['id']]
				);

				$this->Common_models->update_entry(
					'sites_tbl',
					['site_engineer' => $postdata['new_assignee']],
					['site_engineer' => $postdata['id']]
				);

				$this->Common_models->update_entry(
					'site_survey',
					['site_engineer_id' => $postdata['new_assignee']],
					['site_engineer_id' => $postdata['id']]
				);

				$this->Common_models->update_entry(
					'project_tbl',
					['assigned_by' => $postdata['new_assignee']],
					['assigned_by' => $postdata['id']]
				);
			} elseif($postdata['old_role'] == AREA_MANAGER) {
				
				$this->Common_models->update_entry(
					'sites_tbl',
					['area_manager' => $postdata['new_assignee']],
					['area_manager' => $postdata['id']]
				);

				$this->Common_models->update_entry(
					'icr_movement',
					['area_manager_id' => $postdata['new_assignee']],
					['area_manager_id' => $postdata['id']]
				);
			} elseif($postdata['old_role'] == CONTRACTOR) {
				
				$this->Common_models->update_entry(
					'sites_tbl',
					['contractor' => $postdata['new_assignee']],
					['contractor' => $postdata['id']]
				);
			} elseif($postdata['old_role'] == PROJECT_HEAD) {
				
				$this->Common_models->update_entry(
					'workorder_tbl',
					['project_head' => $postdata['new_assignee']],
					['project_head' => $postdata['id']]
				);
			} elseif($postdata['old_role'] == PROJECT_MANAGER) {
				
				$this->Common_models->update_entry(
					'project_tbl',
					['project_manager' => $postdata['new_assignee']],
					['project_manager' => $postdata['id']]
				);
			}

			$this->session->set_flashdata(
				'response',
				'<p class="alert alert-success">Success! Member updated successfully.</p>'
			);
		
		} else {

			$this->session->set_flashdata(
				'response',
				'<p class="alert alert-danger">Failed! unable to update.</p>'
			);
		}

		return redirect('Teams');
	}

	public function delete_user($user_id) {
		
		$results = $this->Common_models->get_entry_row('admin_tbl', ['id' => $user_id]);

		if($results) {

			// check if user have any assosiated project
			if($results['position_type'] == SITE_ENGINEER) {
				
				$res1 = $this->Common_models->get_entry_row(
					'contractor_execution',
					['site_engineer_id' => $user_id]
				);

				$res2 = $this->Common_models->get_entry_row(
					'product',
					['site_eng_id' => $user_id]
				);

				$res3 = $this->Common_models->get_entry_row(
					'sites_tbl',
					['site_engineer' => $user_id]
				);

				$res4 = $this->Common_models->get_entry_row(
					'site_survey',
					['site_engineer_id' => $user_id]
				);

				$res5 = $this->Common_models->get_entry_row(
					'project_tbl',
					['assigned_by' => $user_id]
				);

				if(!$res1 && !$res2 && !$res3 && !$res4 && !$res5) {
					$delete = true;
				} else {
					$delete = false;
				}

			} elseif($results['position_type'] == AREA_MANAGER) {
				
				$res1 = $this->Common_models->get_entry_row(
					'sites_tbl',
					['area_manager' => $user_id]
				);

				$res2 = $this->Common_models->get_entry_row(
					'icr_movement',
					['area_manager_id' => $user_id]
				);

				if(!$res1 && !$res2) {
					$delete = true;
				} else {
					$delete = false;
				}

			} elseif($results['position_type'] == CONTRACTOR) {
				
				$res1 = $this->Common_models->get_entry_row(
					'sites_tbl',
					['contractor' => $user_id]
				);

				if(!$res1) {
					$delete = true;
				} else {
					$delete = false;
				}

			} elseif($results['position_type'] == PROJECT_HEAD) {
				
				$res1 = $this->Common_models->get_entry_row(
					'workorder_tbl',
					['project_head' => $user_id]
				);

				if(!$res1) {
					$delete = true;
				} else {
					$delete = false;
				}

			} elseif($results['position_type'] == PROJECT_MANAGER) {
				
				$res1 = $this->Common_models->get_entry_row(
					'project_tbl',
					['project_manager' => $user_id]
				);

				if(!$res1) {
					$delete = true;
				} else {
					$delete = false;
				}
			}
		}

		if($delete) {

			$this->Common_models->delete_entry('admin_tbl', ['id' => $user_id]);

			$this->session->set_flashdata('response','<p class="alert alert-success">Success! '.DELETE_SUCCESS.'</p>');

		} else {

			$this->session->set_flashdata('response','<p class="alert alert-danger">Error! '.USER_DELETE_ERROR1.'</p>');
		}

		return redirect('Teams');
	}
}
