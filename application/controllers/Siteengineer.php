<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Siteengineer extends MY_Controller {

	function __construct() {

        parent::__construct();
		
		$this->userlogin_type = $this->session->userdata('ses_userlogin_type');
    	$this->user_id = $this->session->userdata('ses_userlogin_id');

		if($this->userlogin_type == CONTRACTOR) {

			$this->site_tbl_role_column = 'contractor';
			$this->siteEngineer_Contractor_id_column = 'contractor_id';

		} else {

			$this->site_tbl_role_column = 'site_engineer';
			$this->siteEngineer_Contractor_id_column = 'site_engineer_id';
		}
    }
	
	public function Projects($isCompletedSites=2, $isCompletedProjects=2) {
		
		$this->session->set_userdata('isCompleted', $isCompletedSites);

		$sel = "SELECT prj.*, wo.order_deadline
				FROM project_tbl AS prj
				LEFT JOIN workorder_tbl AS wo
					ON prj.workorder_id = wo.id
				WHERE prj.id IN (SELECT project_id
									FROM sites_tbl
									WHERE `$this->site_tbl_role_column` = '$this->user_id'
									GROUP BY project_id
									ORDER BY update_date DESC
								)";
		
		if (isset($_GET['filter']) && $_GET['filter'] == 'todayUpdate') {

			$sel .= ' AND DATE(prj.update_date) = CURDATE()';

		} elseif ($isCompletedProjects == 0) { // open projects

			$res = $this->Common_models->getProjectIds();
			if(isset($res['openProject'])) {
				$sel .= ' AND prj.id IN ('.implode(',', array_column($res['openProject'], 'prjId')).')';
			} else {
				$sel .= ' AND prj.id = null';
			}
			
		} elseif ($isCompletedProjects == 1) { // complete projects
			
			$res = $this->Common_models->getProjectIds();
			if(isset($res['completeProject'])) {
				$sel .= ' AND prj.id IN ('.implode(',', array_column($res['completeProject'], 'prjId')).')';
			} else {
				$sel .= ' AND prj.id = null';
			}
		} elseif ($isCompletedSites == 1) { // complete sites
			
			$sel .= " AND prj.id IN (SELECT project_id
									FROM sites_tbl
									WHERE `$this->site_tbl_role_column` = '$this->user_id' AND
									id IN (
											SELECT site_id
											FROM contractor_execution
											WHERE `$this->siteEngineer_Contractor_id_column` = '$this->user_id'
										)
									GROUP BY project_id
									ORDER BY update_date DESC)";
		}

		$q = $this->db->query($sel);
		$res = $q->result_array();
		// echo "<pre>"; print_r($res); die;

		$data['project_list'] = $res;
		$data['isCompletedSites'] = $isCompletedSites;
		$data['isCompletedProjects'] = $isCompletedProjects;

		$this->load->view('admin/common/header');
		$this->load->view('admin/siteengineer_project',$data);
		$this->load->view('admin/common/footer');
	}
	
	public function sites($project_id, $isCompleted) {

		// get circle list
		$data['circle_list'] = $this->Common_models->get_entry(
			'sites_tbl', null, 'circle_name', 'ASC', null, 0, 'DISTINCT(circle_name)'
		);

		// get district list
		$data['district_list'] = $this->Common_models->get_entry(
			'sites_tbl', null, 'land_district', 'ASC', null, 0, 'DISTINCT(land_district)'
		);

		// get taluka list
		$data['taluka_list'] = $this->Common_models->get_entry(
			'sites_tbl', null, 'land_taluka', 'ASC', null, 0, 'DISTINCT(land_taluka)'
		);

		// get village list
		$data['village_list'] = $this->Common_models->get_entry(
			'sites_tbl', null, 'land_village', 'ASC', null, 0, 'DISTINCT(land_village)'
		);

		$user_id=$this->session->userdata('ses_userlogin_id');
		$where1 = array(
			'project_id' => $project_id,
			$this->site_tbl_role_column => $user_id
		);
		$data['sites_list']=$this->Common_models->get_entry('sites_tbl',$where1,'id','DESC',2);
		$data['project_detail']=$this->Common_models->get_entry_row('project_tbl',array('id'=>$project_id));
		$data['isCompleted'] = $isCompleted;
		// echo "<pre>"; print_r($data); die;
		
		$this->load->view('admin/common/header');
		$this->load->view('admin/Siteengineer/sites_list', $data);
		$this->load->view('admin/common/footer');
	}
	
	public function updatesite($site_id) {

		$form_fields = $postdata = $this->input->post();

		unset($form_fields['project_id']);

		$insertdata['form_fields'] = json_encode($form_fields);

		if (count($_FILES) > 0) {

			$files = $this->insert_file('sites_tbl', ['id' => $site_id]);

			if ($files) $insertdata['form_file_fields'] = $files;
		}

		// echo "<pre>"; print_r($insertdata); die;

		$this->Common_models->update_entry('sites_tbl', $insertdata, ['id' => $site_id]);

		// update date only
		$this->Common_models->update_entry('project_tbl', ['update_date' => date('Y-m-d H:i:s')], ['id' => $postdata['project_id']]);
		
		$this->session->set_flashdata('response','<p class="alert alert-success">Success! Data Updated.</p>');

		$isCompleted = $this->session->userdata('isCompleted');

		return redirect('Siteengineer/sites/'.$postdata['project_id'].'/'.$isCompleted);
	}

	public function edit_site($site_id, $isApproved) {

		$data = $this->Common_models->get_entry_row('sites_tbl', ['id' => $site_id]);
		
		$data['form_field_value'] = (array) json_decode($data['form_fields']);
		$data['form_file_field_values'] = (array) json_decode($data['form_file_fields']);

		unset($data['form_fields'], $data['form_file_fields']);

		// get gathering form fields
		$gathering_form_fields = $this->Common_models->get_entry_row('project_tbl',array('id' => $data['project_id']));
		
		$where_in = ['id', json_decode($gathering_form_fields['gathering_form_fields'])];
		$data['gathering_form_fields'] = $this->Common_models->get_entry(
			'form_fields', null, null, null, null, 0, null, $where_in
		);

		$data['isApproved'] = $isApproved;

		// echo "<pre>"; print_r($data); die;

		$this->load->view('admin/common/header');
		$this->load->view('admin/Siteengineer/edit_site_view', $data);
		$this->load->view('admin/common/footer');
	}

	public function sites_server($project_id, $isCompleted) {

		$user_id = $this->session->userdata('ses_userlogin_id');

		$where_string = '';
		if (isset($_GET['circle']) && $_GET['circle'] != 'null' && $_GET['circle'] != '0') {
			$where_string .= ' AND circle_name = "'.$_GET['circle'].'"';
		}
		
		if (isset($_GET['district']) && $_GET['district'] != 'null' && $_GET['district'] != '0') {
			$where_string .= ' AND land_district = "'.$_GET['district'].'"';
		}
		
		if (isset($_GET['taluka']) && $_GET['taluka'] != 'null' && $_GET['taluka'] != '0') {
			$where_string .= ' AND land_taluka = "'.$_GET['taluka'].'"';
		}

		if (isset($_GET['village']) && $_GET['village'] != 'null' && $_GET['village'] != '0') {
			$where_string .= ' AND land_village = "'.$_GET['village'].'"';
		}

		if (isset($_GET['filter']) && $_GET['filter'] == 'todayUpdate') {
			$where_string .= ' AND DATE(update_date) = CURDATE()';
		}

		$get_data = $this->input->get();
		$start = $get_data['start'];
		$limit = $get_data['length'];
		$arrayList = [];
		
		if ($isCompleted == 1) {

			$contSQL = "SELECT COUNT(id) AS totalRecords
						FROM `sites_tbl`
						WHERE
							`project_id` = '$project_id' AND
							`$this->site_tbl_role_column` = '$user_id' AND
							id IN (SELECT site_id
									FROM contractor_execution
									WHERE `$this->siteEngineer_Contractor_id_column` = '$user_id')".
							$where_string;

			$sel = "SELECT *
					FROM `sites_tbl`
					WHERE
						`project_id` = '$project_id' AND
						`$this->site_tbl_role_column` = '$user_id' AND
						id IN (SELECT site_id
								FROM contractor_execution
								WHERE `$this->siteEngineer_Contractor_id_column` = '$user_id')".
						$where_string.
					"ORDER BY update_date DESC
					LIMIT ".$start.", ".$limit;

		} elseif ($isCompleted == 0) {

			$contSQL = "SELECT COUNT(id) AS totalRecords
						FROM `sites_tbl`
						WHERE
							`project_id` = '$project_id' AND
							`$this->site_tbl_role_column` = '$user_id' AND
							id NOT IN (SELECT site_id
										FROM contractor_execution
										WHERE `$this->siteEngineer_Contractor_id_column` = ".$user_id.")".
							$where_string;

			$sel = "SELECT *
					FROM `sites_tbl`
					WHERE
						`project_id` = '$project_id' AND
						`$this->site_tbl_role_column` = '$user_id' AND
						id NOT IN (SELECT site_id
								FROM contractor_execution
								WHERE `$this->siteEngineer_Contractor_id_column` = '$user_id') ".
						$where_string."
					ORDER BY update_date DESC
					LIMIT ".$start.", ".$limit;

		} else {

			$contSQL = "SELECT COUNT(id) AS totalRecords
						FROM `sites_tbl`
						WHERE
							`project_id` = '$project_id' AND
							`$this->site_tbl_role_column` = '$user_id'".
							$where_string;

			$sel = "SELECT *
					FROM `sites_tbl`
					WHERE
						`project_id` = '$project_id' AND
						`$this->site_tbl_role_column` = '$user_id'".
						$where_string."
					ORDER BY update_date DESC
					LIMIT ".$start.", ".$limit;
		}

		// echo $sel; die;
		// get total completed count
		$q = $this->db->query($contSQL);
		$res = $q->row_array();
		$recordsTotal = $res['totalRecords'];

		$q = $this->db->query($sel);
		$result = $q->result_array();

		$i = $this->input->get('start');
		
		// echo "<pre>"; print_r($result); die;
		
		foreach($result as $list) {

			// check list id and user id is available in execution table or not
			$isSurveyUpdated = $this->Common_models->get_entry_row('site_survey', ['site_id' => $list['id']]);

			// check list id and user id is available in survey table or not
			$isExecuted = $this->Common_models->get_entry_row('contractor_execution', ['site_id' => $list['id']]);
			
			if (($isCompleted == 1 && !$isExecuted) || ($isCompleted == 0 && $isExecuted)) {
				continue;
			}

			$site_engineer = $area_manager = $contractor = '';
			$status = GATHERING_PENDING;

			// check list id and user id is available in gather table or not
			$sel = "SELECT id
					FROM `sites_tbl`
					WHERE
						`id` = ".$list['id']." AND (
						site_engineer = '$user_id' OR
						contractor = '$user_id')";
			$q = $this->db->query($sel);
			$isGathered = $q->row_array();

			$action = '<div class="dropdown">
							<button
								class="btn btn-secondary dropdown-toggle"
								type="button"
								id="dropdownMenuButton"
								data-toggle="dropdown"
								aria-haspopup="true"
								aria-expanded="false"
							>Forms</button>
							<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<a class="dropdown-item" href="'.base_url('Siteengineer/edit_site/'.$list['id'].'/'.$list['isApproved']).'">Gathering</a>';
					
			if (count($isGathered) > 0) {

				$status = SURVEY_PENDING;
				$action .= '<a
								class="dropdown-item"
								href="'.base_url('Siteengineer/start_survey/'.$list['id'].'/'.$project_id.'/'.$list['isApproved']).'"
							>Survey</a>';

				if (isset($isSurveyUpdated['id'])) { // survey done block

					$prdMat_URL = 'Product/site_product/'.$list['id'].'/'.$project_id.'/'.$list['isApproved'];

					if($isCompleted == 0) {

						$prdMat_URL .= '?filter=openProject';

					} elseif($isCompleted == 1) {
						
						$prdMat_URL .= '?filter=completeProject';
					
					}
					
					$action .= '<a class="dropdown-item" href="'.base_url($prdMat_URL).'">Product & Material</a>';

					$action .= '<a class="dropdown-item" href="'.base_url('Siteengineer/contractor_execution/'.$list['id'].'/'.$project_id.'/'.$list['isApproved']).'">Execution</a>';
				}
			}

			$action .= '</div></div>';

			if($list['site_engineer']) {

				$whereoo = array('id' => $list['site_engineer']);
				$enter_res = $this->Common_models->get_entry_row('admin_tbl',$whereoo);
				$site_engineer = $enter_res['name'];
			}

			if($list['area_manager']) {

				$whereoo = array('id'=>$list['area_manager']);
				$enter_res = $this->Common_models->get_entry_row('admin_tbl',$whereoo);
				$area_manager = $enter_res['name'];
			}

			if($list['contractor']) {

				$whereoo = array('id'=>$list['contractor']);
				$enter_res = $this->Common_models->get_entry_row('admin_tbl',$whereoo);
				$contractor = $enter_res['name'];
			}

			if (!isset($isExecuted['id'])) {
				
				$status = EXECUTION_PENDING;
				$approveBtn = 'NA';
				
			} elseif($list['isApproved'] == 0) {
				
				$status = NOT_APPROVE_STATUS;
				$approveBtn = '<a class="btn btn-primary" type="button" href="'.base_url('Siteengineer/site_approve/'.$list['id'].'/'.$project_id.'/'.$isCompleted).'">Approve</a>';

			} else { // get ICR movement filled status from Area manager
				
				$approveBtn = 'Approved';

				// get attached ICR form field IDs with project
				$prjDetails = $this->Common_models->get_entry_row('project_tbl', ['id' => $list['project_id']]);
				
				$prj_ICR_form_fields = json_decode($prjDetails['ICR_form_fields']);
				if(
					$prjDetails['ICR_form_fields'] != null &&
					count($prj_ICR_form_fields) > 0
				) {

					// get ICR form fields from static array (i.e. from code base)
					$ICR_form_fields = get_ICR_form_fields();

					foreach ($prj_ICR_form_fields as $value) {
						
						// get ICR filled details by area manager
						$ICR_filled_Details = $this->Common_models->get_entry_row(
							'icr_movement',
							['site_id' => $list['id']]
						);
				
						if($ICR_filled_Details && $ICR_filled_Details[$ICR_form_fields[$value]['name']] == null) {

							$status = $ICR_form_fields[$value]['title']." is pending";
							break;
						} else {
							$status = 'ICR Done';
							continue;
						}
					}
				} elseif($prjDetails['ICR_form_fields'] == null){
					$status = 'ICR N/A';
				}
			}

			$arrayList [] = [
				++$i,
				$list['circle_name'],
				$list['land_district'],
				$list['land_village'],
				$list['land_taluka'],
				$list['workorder_no'],
				$list['beneficiary_id'],
				$list['beneficiary_name'],
				$list['mobilen_number'],
				$list['land_address'],
				$list['pump_load'],
				$list['category'],
				$list['work_order_date'],
				$list['application_status'],
				$list['installation_status'],
				$list['installation_date'],
				$list['remarks'],
				$list['lot'],
				$site_engineer,
				$area_manager,
				$contractor,
				$action,
				$status,
				$approveBtn
			];
		}

		$output = array(
			"draw" 				=> $this->input->get('draw'),
			"recordsTotal" 		=> $recordsTotal,
			"recordsFiltered"	=> $recordsTotal,
			"data" 				=> $arrayList,
		);

		echo json_encode($output);
	}
	
	public function site_approve($site_id, $project_id, $isCompleted) {

		$this->Common_models->update_entry('sites_tbl', ['isApproved' => 1], ['id' => $site_id]);
			
		$this->session->set_flashdata('response','<p class="alert alert-success">Success! site approved.</p>');

		return redirect('Siteengineer/sites/'.$project_id.'/'.$isCompleted);
	}
	
	public function start_survey($site_id, $project_id, $isApproved) {

		$where = array(
			$this->siteEngineer_Contractor_id_column => $this->user_id,
			'site_id' => $site_id
		);
		$data = $this->Common_models->get_entry_row('site_survey', $where, 'id', 'DESC');

		if ($data) {
			
			$data['form_field_value'] = (array) json_decode($data['form_fields']);
			$data['form_file_field_values'] = (array) json_decode($data['form_file_fields']);

			unset($data['form_fields'], $data['form_file_fields']);
		}
		
		$data['site_id'] = $site_id;
		$data['project_id'] = $project_id;
		$data['site_detail'] = $this->Common_models->get_entry_row('sites_tbl', ['id' => $site_id]);

		// get survey form fields
		$survey_form_fields = $this->Common_models->get_entry_row('project_tbl', array('id' => $project_id));
		
		$where_in = ['id', json_decode($survey_form_fields['survey_form_fields'])];
		$data['survey_form_fields'] = $this->Common_models->get_entry(
			'form_fields', null, null, null, null, 0, null, $where_in
		);

		$data['isApproved'] = $isApproved;
		// echo "<pre>"; print_r($data); die;

		$this->load->view('admin/common/header');
		$this->load->view('admin/testn_view', $data);
		$this->load->view('admin/common/footer');
	}

	public function contractor_execution($site_id, $project_id, $isApproved) {

		// get contractor execution data
		$where = array(
			$this->siteEngineer_Contractor_id_column => $this->user_id,
			'site_id' => $site_id
		);
		$data = $this->Common_models->get_entry_row('contractor_execution',$where,'id','DESC');

		if ($data) {
			$data['form_field_value'] = (array) json_decode($data['form_fields']);
			$data['form_file_field_values'] = (array) json_decode($data['form_file_fields']);

			unset($data['form_fields'], $data['form_file_fields']);
		}
		
		// get execution form fields
		$execution_form_fields = $this->Common_models->get_entry_row('project_tbl',array('id' => $project_id));
		
		$where_in = ['id', json_decode($execution_form_fields['execution_form_fields'])];
		$data['execution_form_fields'] = $this->Common_models->get_entry(
			'form_fields', null, null, null, null, 0, null, $where_in
		);

		$data['site_id'] = $site_id;
		$data['project_id'] = $project_id;
		$data['isApproved'] = $isApproved;

		// echo "<pre>"; print_r($data); die;
		
		$this->load->view('admin/common/header');
		$this->load->view('admin/Siteengineer/contractor_execution', $data);
		$this->load->view('admin/common/footer');
	}

	public function add_contractor_execution() {

		$form_fields = $postdata = $this->input->post();

		unset(
			$form_fields['site_id'], 
			$form_fields['id'],
			$form_fields['project_id']
		);

		$insertdata[$this->siteEngineer_Contractor_id_column] = $this->user_id;
		$insertdata['site_id'] = $postdata['site_id'];
		$insertdata['form_fields'] = json_encode($form_fields);

		// echo "<pre>"; print_r($insertdata);
		// echo "<pre>"; print_r($_FILES); 

		if (count($_FILES) > 0) {

			$files = $this->insert_multiple_file('contractor_execution', ['id' => $postdata['id']]);
			// echo "<pre>"; print_r($files); die;

			if ($files) {
				$insertdata['form_file_fields'] = $files;
			}
		}
		
		if (empty($postdata['id'])) {
			
			$this->Common_models->add_entry('contractor_execution', $insertdata);

			$this->session->set_flashdata('response','<p class="alert alert-success">Success! contractor execution updated successfully.</p>');
		} else {

			$this->Common_models->update_entry('contractor_execution', $insertdata, array('id' => $postdata['id']));
			
			$this->session->set_flashdata('response','<p class="alert alert-success">Success! inserted successfully.</p>');
		}

		// update date only
		$this->Common_models->update_entry('project_tbl', ['update_date' => date('Y-m-d H:i:s')], ['id' => $postdata['project_id']]);
		
		return redirect('Siteengineer/sites/'.$postdata['project_id'].'/1');
	}

	private function insert_file($tbl_name, $where) {

		// check for exist file 
		$existFiles = [];
		if(!empty($where['id'])) {
			
			$data = $this->Common_models->get_entry_row($tbl_name, $where);
			$existFiles = $data['form_file_fields'] ? (array) json_decode($data['form_file_fields']) : [];
		}
		
		foreach ($_FILES as $key => $value) {
			
			if ($value['name'] != '') {
				$ext = explode(".", $value['name']);
				$file_name = rand(22,9999).time().".".end($ext);
				
				if(move_uploaded_file($value['tmp_name'], "assets/project_document/$file_name")) $existFiles[$key] = $file_name;
			}
		}	

		if (count($existFiles) > 0) return json_encode($existFiles);
		else return false;
	}

	private function insert_multiple_file($tbl_name, $where) {

		// check for exist file 
		$existFiles = [];
		if(!empty($where['id'])) {
			
			$data = $this->Common_models->get_entry_row($tbl_name, $where);
			$existFiles = $data['form_file_fields'] ? (array) json_decode($data['form_file_fields']) : [];
		}

		foreach ($_FILES as $fileName_key => $values) {
			
			foreach ($values['name'] as $key => $file_name) {
				
				if ($file_name != '') {

					$ext = explode(".", $file_name);
					$file_name = rand(22,9999).time().".".end($ext);
					
					if(move_uploaded_file($values['tmp_name'][$key], "assets/project_document/$file_name")) $existFiles[$fileName_key][] = $file_name;
				}	
			}
		}	

		if (count($existFiles) > 0) return json_encode($existFiles);
		else return false;
	}


	// insert site survay data with dynamic form fields values
	public function add_site_survey()
	{
		$form_fields = $postdata = $this->input->post();

		unset(
			$form_fields['site_id'], 
			$form_fields['id'],
			$form_fields['project_id']
		);

		$insertdata[$this->siteEngineer_Contractor_id_column] = $this->user_id;
		$insertdata['site_id'] = $postdata['site_id'];
		$insertdata['form_fields'] = json_encode($form_fields);

		if (count($_FILES) > 0) {

			$files = $this->insert_file('site_survey', ['id' => $postdata['id']]);

			if ($files) $insertdata['form_file_fields'] = $files;
		}

		// echo "<pre>"; print_r($insertdata); die;

		if (empty($postdata['id'])) {

			$this->Common_models->add_entry('site_survey', $insertdata);

			$this->session->set_flashdata('response','<p class="alert alert-success">Success! site survey added successfully.</p>');

		} else {
			
			$this->Common_models->update_entry('site_survey', $insertdata, ['id' => $postdata['id']]);

			$this->session->set_flashdata('response','<p class="alert alert-success">Success! site survey updated successfully.</p>');
		}

		// update date only
		$this->Common_models->update_entry('project_tbl', ['update_date' => date('Y-m-d H:i:s')], ['id' => $postdata['project_id']]);
		
		$isCompleted = $this->session->userdata('isCompleted');
		
		return redirect('Siteengineer/sites/'.$postdata['project_id'].'/'.$isCompleted);
	}
}
?>