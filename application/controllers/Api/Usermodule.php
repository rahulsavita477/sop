<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usermodule extends CI_Controller {

	function __construct() {

        parent::__construct();

		header("Access-Control-Allow-Origin: *");
		
		$this->post_data=$this->input->post();
		$request_headers = $this->input->request_headers();
	
		if(!isset($request_headers['Userid'])) {
			$res['success']=0;
			$res['msg']='userid required';
  			
  			json_set($res);
  		} else {
  			$this->user_id = $request_headers['Userid'];
  		}
    }

	public function index() {
		echo "ssss"; die;
	}

	public function login()
	{
		$post_data=$this->input->post();
	
		if(empty($post_data['email']) || empty($post_data['password'])) {
			$res['success']=0;
			$res['msg']='Please check parameter';
  		} else {
			$where1=array('email'=>$post_data['email']);
			$userdetail=$this->Common_models->get_entry_row('admin_tbl',$where1);
			if($userdetail)
			{
				if($userdetail['password']==$post_data['password'])
				{
					if($userdetail['status']==1)
					{
						$res['success']=1;
						$res['user_detail']=get_userby_id($userdetail['id']);
						$res['msg']='Login Successfully!';
						json_set($res);
					} else {
						$res['success']=0;
						$res['msg']='Error! Your account blocked by system admin!';
						json_set($res);
					}
				}
			}
			
			$res['success']=0;
			$res['msg']='Invalid Login Detail';
		}
	
		json_set($res);
	}

	public function projects()
	{
		$sel = "select * from project_tbl where id IN (select project_id from sites_tbl where site_engineer='$this->user_id' GROUP BY project_id order by update_date DESC)";
		$q=$this->db->query($sel);
		$result=$q->result_array();
		
		$res['success']=1;
		$res['msg']='projects';
		$res['result']=$result;

		json_set($res);
	}

	public function sites()
	{
		if (!isset($this->post_data['project_id'])) {
			$res['success']=0;
			$res['msg']='project_id required';
  			
  			json_set($res);
		}

		$project_id = $this->post_data['project_id'];
		$page = isset($this->post_data['page']) ? $this->post_data['page'] : 0;
		$limit = isset($this->post_data['limit']) ? $this->post_data['limit'] : 15;

		$start = ($page-1)*$limit;
		$where1=array(
			'project_id'=>$project_id,
			'site_engineer'=>$this->user_id
		);
		
		$arrayList = [];
		$result = $this->Common_models->get_entry('sites_tbl', $where1, 'id', 'DESC', $limit, $start); 		
		$i=0;
		foreach($result as $list) {
			$execution=$gathering=$survey=0;
			$site_engineer=$area_manager=$contractor='';
			
			// check list id and user id is available in gather table or not
			$isGathered=$this->Common_models->get_entry_row('sites_tbl', array('id' => $list['id'], 'site_engineer' => $this->user_id));
			if (count($isGathered) > 0) {
				$gathering=1;

				// check list id and user id is available in execution table or not
				$isSurveyUpdated=$this->Common_models->get_entry_row('site_survey', array('site_id' => $list['id'], 'site_engineer_id' => $this->user_id));

				if (isset($isSurveyUpdated['id'])) {
					$survey=1;

					// check list id and user id is available in survey table or not
					$isExecuted=$this->Common_models->get_entry_row('contractor_execution', array('site_id' => $list['id'], 'site_engineer_id' => $this->user_id));
					if (isset($isExecuted['id'])) {
						$execution = 1;
					} 
				} 
			}

			if($list['site_engineer'])
			{
				$whereoo=array('id'=>$list['site_engineer']);
				$enter_res=$this->Common_models->get_entry_row('admin_tbl',$whereoo);
				$result[$i]['site_engineer']=$enter_res['name'];
			}

			$result[$i]['survey_status'] = $survey;
			$result[$i]['execution_status'] = $execution;
			$result[$i]['gathering'] = $gathering;
			$i++;
		}
		
		$recordsTotal = $this->Common_models->counts_data('sites_tbl',$where1);
		$output = array(
			"pagging" => [
				'total_records' => $recordsTotal,
				'pages' => ceil($recordsTotal/$limit),
				'page' => $page
			],
			"data" => $result,
		);

		json_set($output);
	}

	public function getGatheringForm()
	{
		if (!isset($this->post_data['site_id'])) {
			$res['success'] = 0;
			$res['msg'] = 'site_id required';
  			
  			json_set($res);
		}

		$site_id = $this->post_data['site_id'];

		$where1=array(
			'id'=>$site_id
		);
		$data=$this->Common_models->get_entry_row('sites_tbl', $where1);
		$data['form_values'] = json_decode($data['form_fields']);
		$data['file_values'] = (array) json_decode($data['form_file_fields']);

		unset($data['form_fields'], $data['form_file_fields']);
		$project_id = $data['project_id'];

		// get form fields
		$form_fields = $this->Common_models->get_entry_row('project_tbl',array('id' => $project_id));
		$data['form_fields'] = json_decode($form_fields['gathering_form_fields']);
		$data['attachment_url'] = base_url('assets/project_document/');
		$res['success']=1;
		$res['msg']='form fields with data';
		$res['result']=$data;

		json_set($res);
	}

	public function getForms() {

		if(isset($this->post_data['fieldId'])) {

			$where_in[0] = 'id';
			$where_in[1] = explode(",", $this->post_data['fieldId']);
		} else $where_in=null;

		if(isset($this->post_data['formType'])) $where['form'] = $this->post_data['formType']; 
		else $where = [];
		
		$fields = $this->Common_models->get_entry('form_fields', $where, null, null, null, 0, null, $where_in);
		
		$res['success']=1;
		$res['msg']='form field';
		$res['result']=$fields;

		json_set($res);
	}

	public function getExecutionForm()
	{
		if (!isset($this->post_data['site_id'])) {
			$res['success'] = 0;
			$res['msg'] = 'site_id required';
  			
  			json_set($res);
		}

		$site_id = $this->post_data['site_id'];

		// get project id via sites_tbl
		$data = $this->Common_models->get_entry_row('sites_tbl', ['id' => $site_id]);
		$project_id = $data['project_id'];

		$where = array(
			'site_engineer_id' => $this->user_id,
			'site_id' => $site_id
		);
		$data = $this->Common_models->get_entry_row('contractor_execution', $where);
		
		$data['form_values'] = json_decode($data['form_fields']);
		$data['file_values'] = (array) json_decode($data['form_file_fields']);
		
		unset($data['form_fields'], $data['form_file_fields']);

		$data['ce_id'] = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : '';
		unset($data['id']);

		// get form fields
		$form_fields = $this->Common_models->get_entry_row('project_tbl',array('id' => $project_id));
		$data['form_fields'] = json_decode($form_fields['execution_form_fields']);
		$data['project_id'] = $project_id;
		$data['attachment_url'] = base_url('assets/project_document/');

		$res['success']=1;
		$res['msg']='form fields with data';
		$res['result']=$data;

		json_set($res);
	}

	public function getSurveyForm()
	{
		if (!isset($this->post_data['site_id'])) {
			$res['success'] = 0;
			$res['msg'] = 'site_id required';
  			
  			json_set($res);
		}

		$site_id = $this->post_data['site_id'];

		// get project id via sites_tbl
		$data=$this->Common_models->get_entry_row('sites_tbl', ['id' => $site_id]);
		$project_id = $data['project_id'];

		$where = array(
			'site_engineer_id' => $this->user_id,
			'site_id' => $site_id
		);
		$data = $this->Common_models->get_entry_row('site_survey',$where,'id','DESC');
		
		$data['form_values'] = json_decode($data['form_fields']);
		$data['file_values'] = (array) json_decode($data['form_file_fields']);
		
		unset($data['form_fields'], $data['form_file_fields']);

		// get project form fields
		$survey_form_fields=$this->Common_models->get_entry_row('project_tbl',array('id' => $project_id));
		$data['form_fields'] = json_decode($survey_form_fields['survey_form_fields']);
		
		$data['project_id'] = $project_id;
		$data['survey_id'] = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : '';
		unset($data['id']);
		$data['attachment_url'] = base_url('assets/project_document/');

		$res['success']=1;
		$res['msg']='form fields with data';
		$res['result']=$data;

		json_set($res);
	}

	public function saveGatheringData() {

		if (!isset($this->post_data['site_id'])) {
			$res['success'] = 0;
			$res['msg'] = 'site_id required';
  			
  			json_set($res);
		}

		$site_id = $this->post_data['site_id'];
		unset($this->post_data['site_id']);
		$updatepost = array();
		
		foreach($this->post_data as $key=>$values) {

			if(!empty($this->post_data[$key])) $updatepost[$key]=$values;
		}

		$postData = [];
		$postData['form_fields'] = json_encode($updatepost);

		if (isset($_FILES) && count($_FILES) > 0) {
			
			$files = $this->insert_file('sites_tbl', ['id' => $site_id]);

			if ($files) $postData['form_file_fields'] = $files;
		}

		$this->Common_models->update_entry('sites_tbl', $postData, ['id' => $site_id]);
		
		// update date only
		$prjData = $this->Common_models->get_entry_row('sites_tbl', ['id' => $site_id]);
		$this->Common_models->update_entry('project_tbl', ['update_date' => date('Y-m-d H:i:s')], ['id' => $prjData['project_id']]);
		
		$res['success']=1;
		$res['msg']='updated';

		json_set($res);
	}

	public function saveExecutionData() {

		if (!isset($this->post_data['site_id'])) {
			$res['success'] = 0;
			$res['msg'] = 'site_id required';
  			
  			json_set($res);
		}

		$site_id = $this->post_data['site_id'];
		unset($this->post_data['site_id']);

		// get survey id using site id and site engineer id
		$isExistRecord = $this->Common_models->get_entry_row('contractor_execution', ['site_engineer_id' => $this->user_id, 'site_id' => $site_id]);
		
		foreach($this->post_data as $key=>$values) {

			if(!empty($this->post_data[$key])) $updatepost[$key]=$values;
		}

		$postData = [];
		$ceId = isset($isExistRecord['id']) ? $isExistRecord['id'] : null;
		$postData['form_fields'] = json_encode($updatepost);

		if (isset($_FILES) && count($_FILES) > 0) {
			
			$files = $this->insert_multiple_file('contractor_execution', ['id' => $ceId]);

			if ($files) $postData['form_file_fields'] = $files;
		}
		
		$postData['site_engineer_id'] = $this->user_id;
		$postData['site_id'] = $site_id;
		$postData['create_date'] = date('Y-m-d H:i:s');

		if ($ceId) {
			
			$this->Common_models->update_entry('contractor_execution', $postData, ['id' => $isExistRecord['id']]);
			$res['msg'] = 'updated';
		
		} else {
			
			$this->Common_models->add_entry('contractor_execution', $postData);
			$res['msg'] = 'inserted';
		}

		// update date only
		$prjData = $this->Common_models->get_entry_row('sites_tbl', ['id' => $site_id]);
		$this->Common_models->update_entry('project_tbl', ['update_date' => date('Y-m-d H:i:s')], ['id' => $prjData['project_id']]);
		
		$res['success'] = 1;
		json_set($res);
	}

	public function saveSurveyData() {

		if (!isset($this->post_data['site_id'])) {
			$res['success'] = 0;
			$res['msg'] = 'site_id required';
  			
  			json_set($res);
		}

		$site_id = $this->post_data['site_id'];
		unset($this->post_data['site_id']);

		// get survey id using site id and site engineer id
		$isExistRecord = $this->Common_models->get_entry_row('site_survey', ['site_engineer_id' => $this->user_id, 'site_id' => $site_id]);

		foreach($this->post_data as $key=>$values) {

			if(!empty($this->post_data[$key])) $updatepost[$key]=$values;
		}

		$postData = [];
		$surveyId = isset($isExistRecord['id']) ? $isExistRecord['id'] : null;
		$postData['form_fields'] = json_encode($updatepost);

		if (isset($_FILES) && count($_FILES) > 0) {
			
			$files = $this->insert_file('site_survey', ['id' => $surveyId]);

			if ($files) $postData['form_file_fields'] = $files;
		}

		$postData['site_engineer_id'] = $this->user_id;
		$postData['site_id'] = $site_id;
		$postData['create_date'] = date('Y-m-d H:i:s');

		if (isset($surveyId)) {
			
			$this->Common_models->update_entry('site_survey', $postData, ['id' => $surveyId]);
			$res['msg'] = 'updated';
		
		} else {
			
			$this->Common_models->add_entry('site_survey', $postData);
			$res['msg'] = 'inserted';
		}

		// update date only
		$prjData = $this->Common_models->get_entry_row('sites_tbl', ['id' => $site_id]);
		$this->Common_models->update_entry('project_tbl', ['update_date' => date('Y-m-d H:i:s')], ['id' => $prjData['project_id']]);
		
		$res['success']=1;
		json_set($res);
	}

	private function insert_multiple_file($tbl_name, $where) {

		// check for exist file 
		$existFiles = [];
		if(!empty($where['id'])) {
			
			$data = $this->Common_models->get_entry_row($tbl_name, $where);
			$existFiles = $data['form_file_fields'] ? (array) json_decode($data['form_file_fields']) : [];
		}

		foreach ($_FILES as $fileName_key => $values) {
			
			foreach ($values['name'] as $key => $file_name) {
				
				if ($file_name != '') {

					$ext = explode(".", $file_name);
					$file_name = rand(22,9999).time().".".end($ext);
					
					if(move_uploaded_file($values['tmp_name'][$key], "assets/project_document/$file_name")) $existFiles[$fileName_key][] = $file_name;
				}	
			}
		}	

		if (count($existFiles) > 0) return json_encode($existFiles);
		else return false;
	}

	private function insert_file($tbl_name, $where) {

		// check for exist file 
		$existFiles = [];
		if(!empty($where['id'])) {
			
			$data = $this->Common_models->get_entry_row($tbl_name, $where);
			$existFiles = $data['form_file_fields'] ? (array) json_decode($data['form_file_fields']) : [];
		}
		
		foreach ($_FILES as $key => $value) {
			
			if ($value['name'] != '') {

				$ext = explode(".", $value['name']);
				$file_name = rand(22,9999).time().".".end($ext);
				
				if(move_uploaded_file($value['tmp_name'], "assets/project_document/$file_name")) $existFiles[$key] = $file_name;
			}
		}	

		if (count($existFiles) > 0) return json_encode($existFiles);
		else return false;
	}
	
	public function getProductMaterial()
	{
		if (!isset($this->post_data['site_id'])) {
			$res['success'] = 0;
			$res['msg'] = 'site_id required';
  			
  			json_set($res);
		}

		if (!isset($this->post_data['project_id'])) {
			$res['success'] = 0;
			$res['msg'] = 'project_id required';
  			
  			json_set($res);
		}

		$data = $this->Common_models->get_entry_row('product', ['project_id' => $this->post_data['site_id'], 'site_id' => $this->post_data['project_id'], 'site_eng_id' => $this->user_id], 'id', 'DESC');

		if (!$data) {
			// get default product data
			$data = $this->Common_models->get_entry_row('product_default_values',['project_id' => $this->post_data['project_id']],'id','DESC');
		}

		$res['success'] = 1;
		$res['msg'] = 'product material';
		$res['result'] = $data;

		json_set($res);
	}	

	public function saveProductMaterial()
	{
		$postdata=$this->input->post();

		$insertdata['project_id']=$postdata['project_id'];
		$insertdata['product_name']=$postdata['product_name'];
		$insertdata['pump_capacity']=$postdata['pump_capacity'];
		$insertdata['pump_type']=$postdata['pump_type'];
		$insertdata['power_type']=$postdata['power_type'];
		$insertdata['pump_head']=$postdata['pump_head'];
		$insertdata['structure_height']=$postdata['structure_height'];
		$insertdata['Panel_capacity']=$postdata['panel_capacity'];
		$insertdata['structure_staging']=$postdata['structure_staging'];
		$insertdata['water_tank_capacity']=$postdata['water_tank_capacity'];
		$insertdata['foundation_bolt']=$postdata['foundation_bolt'];
		$insertdata['civil_poles']=$postdata['civil_poles'];
		$insertdata['cement']=$postdata['cement'];
		$insertdata['tmt_8mm']=$postdata['tmt_8mm'];
		$insertdata['tmt_10mm']=$postdata['tmt_10mm'];
		$insertdata['tmt_12mm']=$postdata['tmt_12mm'];
		$insertdata['structure_material']=$postdata['structure_material'];
		$insertdata['bos_material']=$postdata['bos_material'];
		$insertdata['sumbersible_pump']=$postdata['sumbersible_pump'];
		$insertdata['controllers']=$postdata['controllers'];
		$insertdata['water_tank']=$postdata['water_tank'];
		$insertdata['solar_panel']=$postdata['solar_panel'];
		$insertdata['hdpe_pipe_length']=$postdata['hdpe_pipe_length'];
		$insertdata['wire_rope_length']=$postdata['wire_rope_length'];
		$insertdata['power_cable_length']=$postdata['power_cable_length'];
		$insertdata['create_date']=date('Y-m-d H:i:s');
		
		// insert or update site values
		$where = array(
			'project_id' => $postdata['project_id'],
			'site_id' => $postdata['site_id'],
			'site_eng_id' => $this->user_id
		);
		$data = $this->Common_models->get_entry_row('product', $where, 'id', 'DESC');
		
		if ($data) { // update record
			$add_data = $this->Common_models->update_entry('product', $insertdata, array('id' => $data['id']));

			$res['msg'] = 'updated';
		} else { // insert record
			$insertdata['site_id'] = $postdata['site_id'];
			$insertdata['site_eng_id'] = $this->user_id;

			$add_data = $this->Common_models->add_entry('product', $insertdata);

			$res['msg'] = 'inserted';
		}

		// update date only
		$this->Common_models->update_entry('project_tbl', ['update_date' => date('Y-m-d H:i:s')], ['id' => $postdata['project_id']]);
		
		$res['success'] = 1;
		json_set($res);
	}
}
?>