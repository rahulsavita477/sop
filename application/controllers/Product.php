<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product extends MY_Controller {

	function __construct() {

        parent::__construct();

        $this->userlogin_type = $this->session->userdata('ses_userlogin_type');
    	$this->user_id = $this->session->userdata('ses_userlogin_id');
    }
	
	public function add_product($project_id) {

		$data = $this->Common_models->get_entry_row('product_default_values', ['project_id' => $project_id], 'id', 'DESC');
		// echo "<pre>"; print_r($data); die;

		$data['project_id'] = $project_id;

		$this->load->view('admin/common/header');
		$this->load->view('admin/add-products', $data);
		$this->load->view('admin/common/footer');
	}

	public function site_product($site_id, $project_id, $isApproved) {

		$where = array(
			'project_id' => $project_id,
			'site_id' => $site_id,
			'site_eng_id' => $this->user_id
		);
		$data = $this->Common_models->get_entry_row('product',$where,'id','DESC');
		
		if (!$data) {

			// get default product data
			$data = $this->Common_models->get_entry_row('product_default_values',['project_id' => $project_id],'id','DESC');
		}

		// echo "<pre>"; print_r($data); die;
		$data['project_id'] = $project_id;
		$data['site_id'] = $site_id;
		$data['isApproved'] = $isApproved;

		$this->load->view('admin/common/header');
		$this->load->view('admin/Areamanager/add-site-products', $data);
		$this->load->view('admin/common/footer');
	}

	public function save_product() {

		$postdata = $this->input->post();

		$insertdata['project_id'] = $postdata['project_id'];
		$insertdata['product_name'] = $postdata['product_name'];
		$insertdata['pump_capacity'] = $postdata['pump_capacity'];
		$insertdata['pump_type'] = $postdata['pump_type'];
		$insertdata['power_type'] = $postdata['power_type'];
		$insertdata['pump_head'] = $postdata['pump_head'];
		$insertdata['structure_height'] = $postdata['structure_height'];
		$insertdata['Panel_capacity'] = $postdata['Panel_capacity'];
		$insertdata['structure_staging'] = $postdata['structure_staging'];
		$insertdata['water_tank_capacity'] = $postdata['water_tank_capacity'];
		$insertdata['foundation_bolt'] = $postdata['foundation_bolt'];
		$insertdata['civil_poles'] = $postdata['civil_poles'];
		$insertdata['cement'] = $postdata['cement'];
		$insertdata['tmt_8mm'] = $postdata['tmt_8mm'];
		$insertdata['tmt_10mm'] = $postdata['tmt_10mm'];
		$insertdata['tmt_12mm'] = $postdata['tmt_12mm'];
		$insertdata['structure_material'] = $postdata['structure_material'];
		$insertdata['bos_material'] = $postdata['bos_material'];
		$insertdata['sumbersible_pump'] = $postdata['sumbersible_pump'];
		$insertdata['controllers'] = $postdata['controllers'];
		$insertdata['water_tank'] = $postdata['water_tank'];
		$insertdata['solar_panel'] = $postdata['solar_panel'];
		$insertdata['hdpe_pipe_length'] = $postdata['hdpe_pipe_length'];
		$insertdata['wire_rope_length'] = $postdata['wire_rope_length'];
		$insertdata['power_cable_length'] = $postdata['power_cable_length'];
		$insertdata['create_date'] = date('Y-m-d H:i:s');

		// insert or update default product values 
		if (empty($postdata['id'])) {
			$add_data = $this->Common_models->add_entry('product_default_values', $insertdata);
		} else {
			$add_data = $this->Common_models->update_entry('product_default_values', $insertdata, ['id' => $postdata['id']]);
		}

		// update date only
		$this->Common_models->update_entry(
			'project_tbl', ['update_date' => date('Y-m-d H:i:s')], ['id' => $postdata['project_id']]
		);
		
		if ($add_data) {
			$this->session->set_flashdata(
				'response','<p class="alert alert-success">Success! product updated successfully.</p>'
			);
		} else {
			$this->session->set_flashdata('response','<p class="alert alert-danger">Failed! unable to add product.</p>');
		}

		return redirect('Projects');
	}

	public function save_site_product()
	{
		$postdata=$this->input->post();

		$insertdata['project_id'] = $postdata['project_id'];
		$insertdata['product_name'] = $postdata['product_name'];
		$insertdata['pump_capacity'] = $postdata['pump_capacity'];
		$insertdata['pump_type'] = $postdata['pump_type'];
		$insertdata['power_type'] = $postdata['power_type'];
		$insertdata['pump_head'] = $postdata['pump_head'];
		$insertdata['structure_height'] = $postdata['structure_height'];
		$insertdata['Panel_capacity'] = $postdata['Panel_capacity'];
		$insertdata['structure_staging'] = $postdata['structure_staging'];
		$insertdata['water_tank_capacity'] = $postdata['water_tank_capacity'];
		$insertdata['foundation_bolt'] = $postdata['foundation_bolt'];
		$insertdata['civil_poles'] = $postdata['civil_poles'];
		$insertdata['cement'] = $postdata['cement'];
		$insertdata['tmt_8mm'] = $postdata['tmt_8mm'];
		$insertdata['tmt_10mm'] = $postdata['tmt_10mm'];
		$insertdata['tmt_12mm'] = $postdata['tmt_12mm'];
		$insertdata['structure_material'] = $postdata['structure_material'];
		$insertdata['bos_material'] = $postdata['bos_material'];
		$insertdata['sumbersible_pump'] = $postdata['sumbersible_pump'];
		$insertdata['controllers'] = $postdata['controllers'];
		$insertdata['water_tank'] = $postdata['water_tank'];
		$insertdata['solar_panel'] = $postdata['solar_panel'];
		$insertdata['hdpe_pipe_length'] = $postdata['hdpe_pipe_length'];
		$insertdata['wire_rope_length'] = $postdata['wire_rope_length'];
		$insertdata['power_cable_length'] = $postdata['power_cable_length'];
		$insertdata['create_date'] = date('Y-m-d H:i:s');
		
		// insert or update site values
		$where = array(
			'project_id' => $postdata['project_id'],
			'site_id' => $postdata['site_id'],
			'site_eng_id' => $this->user_id
		);
		$data = $this->Common_models->get_entry_row('product',$where,'id','DESC');
		
		if ($data) { // update record
			$add_data = $this->Common_models->update_entry('product', $insertdata, array('id' => $postdata['id']));
		} else { // insert record
			$insertdata['site_id'] = $postdata['site_id'];
			$insertdata['site_eng_id'] = $this->user_id;

			$add_data=$this->Common_models->add_entry('product', $insertdata);
		}

		// update date only
		$this->Common_models->update_entry(
			'project_tbl', ['update_date' => date('Y-m-d H:i:s')], ['id' => $postdata['project_id']]
		);
		
		if ($add_data) {
			$this->session->set_flashdata(
				'response','<p class="alert alert-success">Success! product updated successfully.</p>'
			);
		} else {
			$this->session->set_flashdata('response','<p class="alert alert-danger">Failed! unable to add product.</p>');
		}

		$url = 'Siteengineer/sites/'.$postdata['project_id'].'/2';
		if(isset($postdata['filter'])) {
			
			if($postdata['filter'] == 'completeProject') {
				$url = 'Siteengineer/sites/'.$postdata['project_id'].'/1';
			} elseif($postdata['filter'] == 'openProject') {
				$url = 'Siteengineer/sites/'.$postdata['project_id'].'/0';
			}
		} 

		return redirect($url);
	}
}
