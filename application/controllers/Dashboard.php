<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	function __construct() {

        parent::__construct();

		$this->userlogin_type = $this->session->userdata('ses_userlogin_type');
    	$this->user_id = $this->session->userdata('ses_userlogin_id');
    }

	public function index()
	{
		$data['role'] = $this->userlogin_type;

		switch ($data['role']) {

			case SITE_ENGINEER: {

				$data['counts_data'] = $this->getSiteEngineerCounts();

				break;
			}
			
			case CONTRACTOR: {

				$data['counts_data'] = $this->getContractorCounts();

				break;
			}

			case OPERATION_HEAD: {

				$data['counts_data'] = $this->getOperationHeadCounts();

				break;
			}

			case PROJECT_HEAD: {

				$data['counts_data'] = $this->getProjectHeadCounts();

				break;
			}

			case PROJECT_MANAGER: {

				$data['counts_data'] = $this->getProjectManagerCounts();

				break;
			}

			case ADMIN: {

				$data['counts_data'] = $this->getAdminCounts();

				break;
			}

			case AREA_MANAGER: {

				$data['counts_data'] = $this->getAreaManagerCounts();

				break;
			}
		}

		$data['counts_data']['users']['count'] = $this->Common_models->counts_data('admin_tbl', null, 'id');
		$data['counts_data']['users']['url'] = 'Teams';

		// echo "<pre>"; print_r($data); die;

		$this->load->view('admin/common/header');
		$this->load->view('admin/dashboard_view', $data);
		$this->load->view('admin/common/footer');
	}

	public function getSiteEngineerCounts() {

		// define variables
		$count_result = [];
		$where1 = [];
		$where2 = [];

		// get total projects
		$sel = "SELECT id
				FROM project_tbl 
				WHERE id IN (SELECT project_id
							FROM sites_tbl
							WHERE site_engineer = '$this->user_id'
							GROUP BY project_id)";
		$q = $this->db->query($sel);
		$res = $q->result_array();

		$count_result['total_projects']['count'] = count($res);
		$count_result['total_projects']['url'] = 'Siteengineer/Projects';

		//-- get completed projects
			// get all projects
		// echo "<pre>"; print_r($res);
		$totalCompletedProjects = 0;
		$totalOpenProjects = 0;

		if (count($res)>0) {
			foreach ($res as $key => $value) {

				// get total assigned sites
				$where1['project_id'] = $value['id'];
				$where1['site_engineer'] = $this->user_id;
				$totalAssignedSites = $this->Common_models->counts_data('sites_tbl', $where1);
				// echo "<pre>"; print_r($count_data);

				// get completed assigned sites
				$contSQL = "SELECT COUNT(id) AS completedAssignedSites
							FROM `sites_tbl`
							WHERE 
								`project_id` = ".$value['id']." AND
								`site_engineer` = '$this->user_id' AND
								id IN (SELECT site_id
										FROM contractor_execution
										WHERE site_engineer_id = '$this->user_id'
									)";
				$q = $this->db->query($contSQL);
				$res = $q->row_array();
				// echo "<pre>"; print_r($res['completedAssignedSites']);

				if ($totalAssignedSites != 0 && $totalAssignedSites == $res['completedAssignedSites']) {
					$totalCompletedProjects++;
				} else {
					$totalOpenProjects++;
				}
			}	
		}

		$count_result['total_completed_projects']['count'] = $totalCompletedProjects;
		$count_result['total_completed_projects']['url'] = 'Siteengineer/Projects/1/1';

		$count_result['total_open_projects']['count'] = $totalOpenProjects;
		$count_result['total_open_projects']['url'] = 'Siteengineer/Projects/2/0';

		// get today update count
		$todaysUpdateCountSQL = "SELECT COUNT(id) AS totalRecords
								FROM project_tbl
								WHERE id IN (SELECT project_id
											FROM sites_tbl
											WHERE site_engineer = '$this->user_id'
											GROUP BY project_id) AND
									DATE(update_date) = CURDATE()";
		$q = $this->db->query($todaysUpdateCountSQL);
		$res = $q->row_array();

		$count_result['today_update']['count'] = $res['totalRecords'];
		$count_result['today_update']['url'] = 'Siteengineer/Projects?filter=todayUpdate';

		// echo "<pre>"; print_r($count_result); die;
		return $count_result;
	}

	public function getContractorCounts() {

		// define variables
		$count_result = [];
		$where1 = [];
		$where2 = [];

		// get total projects
		$sel = "SELECT id
				FROM project_tbl 
				WHERE id IN (SELECT project_id
							FROM sites_tbl
							WHERE contractor = '$this->user_id'
							GROUP BY project_id)";
		$q = $this->db->query($sel);
		$res = $q->result_array();

		$count_result['total_projects']['count'] = count($res);
		$count_result['total_projects']['url'] = 'Siteengineer/Projects';

		//-- get completed projects
			// get all projects
		// echo "<pre>"; print_r($res);
		$totalCompletedProjects = 0;
		$totalOpenProjects = 0;

		if (count($res)>0) {

			foreach ($res as $key => $value) {

				// get total assigned sites
				$where1['project_id'] = $value['id'];
				$where1['contractor'] = $this->user_id;
				$totalAssignedSites = $this->Common_models->counts_data('sites_tbl', $where1);
				// echo "<pre>"; print_r($count_data);

				// get completed assigned sites
				$contSQL = "SELECT COUNT(id) AS completedAssignedSites
							FROM `sites_tbl`
							WHERE 
								`project_id` = ".$value['id']." AND
								`contractor` = '$this->user_id' AND
								id IN (SELECT site_id
										FROM contractor_execution
										WHERE contractor_id = '$this->user_id'
									)";
				$q = $this->db->query($contSQL);
				$res = $q->row_array();
				// echo "<pre>"; print_r($res['completedAssignedSites']);

				if ($totalAssignedSites != 0 && $totalAssignedSites == $res['completedAssignedSites']) {
					$totalCompletedProjects++;
				} else {
					$totalOpenProjects++;
				}
			}
		}

		$count_result['total_completed_projects']['count'] = $totalCompletedProjects;
		$count_result['total_completed_projects']['url'] = 'Siteengineer/Projects/1/1';

		$count_result['total_open_projects']['count'] = $totalOpenProjects;
		$count_result['total_open_projects']['url'] = 'Siteengineer/Projects/2/0';

		// get today update count
		$todaysUpdateCountSQL = "SELECT COUNT(id) AS totalRecords
								FROM project_tbl
								WHERE id IN (SELECT project_id
											FROM sites_tbl
											WHERE contractor = '$this->user_id'
											GROUP BY project_id) AND
									DATE(update_date) = CURDATE()";
		$q = $this->db->query($todaysUpdateCountSQL);
		$res = $q->row_array();

		$count_result['today_update']['count'] = $res['totalRecords'];
		$count_result['today_update']['url'] = 'Siteengineer/Projects?filter=todayUpdate';

		// echo "<pre>"; print_r($count_result); die;
		return $count_result;
	}

	public function getOperationHeadCounts() {
		
		// define variables
		$count_result = [];
		$where1 = [];

		// get total projects
		$sel = "SELECT id 
				FROM project_tbl 
				WHERE workorder_id IN (SELECT id
										FROM workorder_tbl
										WHERE created_by = '$this->user_id')";
		$q = $this->db->query($sel);
		$res = $q->result_array();

		$count_result['total_projects']['count'] = count($res);
		$count_result['total_projects']['url'] = 'Report';

		//-- get completed projects
			// get all projects
		// echo "<pre>"; print_r($res); die;
		$totalCompletedProjects = 0;
		$totalOpenProjects = 0;

		if (count($res)>0) {
			foreach ($res as $key => $value) {

				// get total project sites
				$where1['project_id'] = $value['id'];
				$totalProjectSites = $this->Common_models->get_entry('sites_tbl', $where1, null, null, null, 0, 'id');
				$projectSitesID = array_column($totalProjectSites, 'id');
				$totalProjectSites = count($projectSitesID);

				// get completed sites
				if(count($projectSitesID) > 0) {
				 
				    $sel = "SELECT count(id) AS completedSites
                            FROM contractor_execution 
                            WHERE site_id IN (".implode(',', $projectSitesID).")";
				    $q = $this->db->query($sel);
				    $totalExecutedProjects = $q->row_array();   
				} else {
				    $totalExecutedProjects = [];
				}

				if (count($totalExecutedProjects) > 0 && $totalProjectSites == $totalExecutedProjects['completedSites']) $totalCompletedProjects++;
				else $totalOpenProjects++;
			}
		}

		// $count_result['total_completed_projects']['count'] = $totalCompletedProjects;
		// $count_result['total_completed_projects']['url'] = 'Siteengineer/Projects/0';

		// $count_result['total_open_projects']['count'] = $totalOpenProjects;
		// $count_result['total_open_projects']['url'] = 'Siteengineer/Projects/0';

		$count_result['total_completed_projects']['count'] = $totalCompletedProjects;
		$count_result['total_completed_projects']['url'] = 'Report?filter=completeProject';

		$count_result['total_open_projects']['count'] = $totalOpenProjects;
		$count_result['total_open_projects']['url'] = 'Report?filter=openProject';

		// get total work order
		$sql = "SELECT COUNT(wo.id) AS totalRecords 
				FROM workorder_tbl AS wo
				LEFT JOIN project_tbl 
					ON workorder_id = wo.id
				WHERE wo.created_by = '$this->user_id'
				ORDER BY wo.update_date DESC";
		$q = $this->db->query($sql);
		$totalWorkOrder = $q->row_array();

		$count_result['total_workorder']['count'] = $totalWorkOrder['totalRecords'];
		$count_result['total_workorder']['url'] = 'Workorder';

		// today report count
		$todayReportCount = 0;
		if (count($res)>0) {

			$projectIDs = array_column($res, 'id');
			$sql = "SELECT COUNT(id) AS totalRecords 
					FROM project_tbl
					WHERE 
						DATE(update_date) = DATE(CURRENT_DATE) AND
						id IN (".implode(',', $projectIDs).")";
			$q = $this->db->query($sql);
			$todayReportCount = $q->row_array();
			$todayReportCount = $todayReportCount['totalRecords'];
		}

		$count_result['today_report']['count'] = $todayReportCount;
		// $count_result['today_report']['url'] = '?filter=todayUpdate';
		$count_result['today_report']['url'] = 'Report?filter=todayUpdate';

		// echo "<pre>"; print_r($count_result); die;
		return $count_result;
	}	

	public function getProjectHeadCounts()
	{	
		// define variables
		$count_result = [];
		$where1 = [];

		// get total projects
		// get project list
		$sql = "SELECT prj.id
				FROM project_tbl AS prj
				WHERE prj.created_by = '$this->user_id'";
		$q = $this->db->query($sql);
		$res = $q->result_array();

		$count_result['total_projects']['count'] = count($res);
		$count_result['total_projects']['url'] = 'Projects';

		//-- get completed projects
			// get all projects
		// echo "<pre>"; print_r($res); die;
		$totalCompletedProjects = 0;
		$totalOpenProjects = 0;

		if (count($res)>0) {

			foreach ($res as $key => $value) {

				// get total project sites
				$where1['project_id'] = $value['id'];
				$totalProjectSites = $this->Common_models->get_entry('sites_tbl', $where1, null, null, null, 0, 'id');
				$projectSitesID = array_column($totalProjectSites, 'id');
				$totalProjectSites = count($projectSitesID);

				// get completed sites
				if(count($projectSitesID)>0) {

					$sel = "SELECT count(id) AS completedSites
						FROM contractor_execution 
						WHERE site_id IN (".implode(',', $projectSitesID).")";
					$q = $this->db->query($sel);
					$totalExecutedProjects = $q->row_array();
				} else {
					$totalExecutedProjects = [];
				}
				
				if (count($totalExecutedProjects) > 0 && $totalProjectSites == $totalExecutedProjects['completedSites']) $totalCompletedProjects++;
				else $totalOpenProjects++;
			}
		}

		$count_result['total_completed_projects']['count'] = $totalCompletedProjects;
		$count_result['total_completed_projects']['url'] = 'Projects?filter=completeProject';

		$count_result['total_open_projects']['count'] = $totalOpenProjects;
		$count_result['total_open_projects']['url'] = 'Projects?filter=openProject';

		// today report count
		$todayReportCount = 0;
		if (count($res)>0) {

			$projectIDs = array_column($res, 'id');
			$sql = "SELECT COUNT(id) AS totalRecords 
					FROM project_tbl
					WHERE 
						DATE(update_date) = DATE(CURRENT_DATE) AND
						id IN (".implode(',', $projectIDs).")";
			$q = $this->db->query($sql);
			$todayReportCount = $q->row_array();
			$todayReportCount = $todayReportCount['totalRecords'];
		}

		$count_result['today_report']['count'] = $todayReportCount;
		$count_result['today_report']['url'] = 'Projects?filter=todayUpdate';

		// echo "<pre>"; print_r($count_result); die;
		return $count_result;
	}

	public function getProjectManagerCounts()
	{	
		// define variables
		$count_result = [];
		$where1 = [];

		// get total projects
		// get project list
		$sql = "SELECT prj.*, wo.order_deadline 
    			FROM project_tbl AS prj
    			INNER JOIN workorder_tbl AS wo 
    				ON workorder_id = wo.id
    			WHERE assigned = 1 AND 
    				FIND_IN_SET(".$this->user_id.", project_manager)";
		$q = $this->db->query($sql);
		$res = $q->result_array();

		$count_result['total_projects']['count'] = count($res);
		$count_result['total_projects']['url'] = 'Projectmanager/projects';

		//-- get completed projects
			// get all projects
// 		echo "<pre>"; print_r($res); die;
		$totalCompletedProjects = 0;
		$totalOpenProjects = 0;

		if (count($res)>0) {

			foreach ($res as $key => $value) {

				// get total project sites
				$where1['project_id'] = $value['id'];
				$totalProjectSites = $this->Common_models->counts_data('sites_tbl', $where1);

				// get assigned sites
				$where = [];
				$where['project_id'] = $value['id'];
				$where['assigned'] = 1;
				$totalAssignedSites = $this->Common_models->counts_data('sites_tbl', $where);

				if ($totalProjectSites != 0 && $totalProjectSites == $totalAssignedSites) {
					$totalCompletedProjects++;
				} else {
					$totalOpenProjects++;
				}
			}
		}

		$count_result['total_completed_projects']['count'] = $totalCompletedProjects;
		$count_result['total_completed_projects']['url'] = 'Projectmanager/projects?filter=completeProject';

		$count_result['total_open_projects']['count'] = $totalOpenProjects;
		$count_result['total_open_projects']['url'] = 'Projectmanager/projects?filter=openProject';

		// today report count
		$todayReportCount = 0;
		if (count($res)>0) {

			$projectIDs = array_column($res, 'id');
			$sql = "SELECT COUNT(id) AS totalRecords 
					FROM project_tbl
					WHERE 
						DATE(update_date) = DATE(CURRENT_DATE) AND
						id IN (".implode(',', $projectIDs).")";
			$q = $this->db->query($sql);
			$todayReportCount = $q->row_array();
			$todayReportCount = $todayReportCount['totalRecords'];
		}

		$count_result['today_report']['count'] = $todayReportCount;
		$count_result['today_report']['url'] = 'Projectmanager/projects?filter=todayUpdate';

		// echo "<pre>"; print_r($count_result); die;
		return $count_result;
	}	

	public function getAdminCounts()
	{	
		// define variables
		$count_result = [];
		$where1 = [];

		// get total projects
		// get project list
		$sql = "SELECT id FROM project_tbl";
		$q = $this->db->query($sql);
		$res = $q->result_array();

		$count_result['total_projects']['count'] = count($res);
		$count_result['total_projects']['url'] = 'Report';

		//-- get completed projects
			// get all projects
		// echo "<pre>"; print_r($res); die;
		$totalCompletedProjects = 0;
		$totalOpenProjects = 0;

		if (count($res)>0) {
		
			foreach ($res as $key => $value) {

				// echo "project id: ".$value['id'];

				// get total project sites
				$where1['project_id'] = $value['id'];
				$totalProjectSites = $this->Common_models->get_entry('sites_tbl', $where1, null, null, null, 0, 'id');
				$projectSitesID = array_column($totalProjectSites, 'id');
				$totalProjectSites = count($projectSitesID);
				// echo "<pre>"; print_r($projectSitesID); die;

				// get completed sites
				if (count($projectSitesID)>0) {
					$sel = "SELECT count(id) AS completedSites
							FROM contractor_execution 
							WHERE site_id IN (".implode(',', $projectSitesID).")";
					$q = $this->db->query($sel);
					$totalExecutedProjects = $q->row_array();
				} else {
					$totalExecutedProjects['completedSites'] = 0;
				}			

				if ($totalProjectSites != 0 && $totalProjectSites == $totalExecutedProjects['completedSites']) {
					$totalCompletedProjects++;
				} else {
					$totalOpenProjects++;
				}
			}
		}

		$count_result['total_completed_projects']['count'] = $totalCompletedProjects;
		$count_result['total_completed_projects']['url'] = 'Report?filter=completeProject';

		$count_result['total_open_projects']['count'] = $totalOpenProjects;
		$count_result['total_open_projects']['url'] = 'Report?filter=openProject';

		// today report count
		$todayReportCount = 0;
		if (count($res)>0) {

			$projectIDs = array_column($res, 'id');
			$sql = "SELECT COUNT(id) AS totalRecords 
					FROM project_tbl
					WHERE 
						DATE(update_date) = DATE(CURRENT_DATE) AND
						id IN (".implode(',', $projectIDs).")";
			$q = $this->db->query($sql);
			$todayReportCount = $q->row_array();
			$todayReportCount = $todayReportCount['totalRecords'];
		}

		$count_result['today_report']['count'] = $todayReportCount;
		$count_result['today_report']['url'] = 'Report?filter=todayUpdate';

		// get total work order
		$sql = "SELECT COUNT(wo.id) AS totalRecords 
				FROM workorder_tbl AS wo
				LEFT JOIN project_tbl 
					ON workorder_id = wo.id";
		$q = $this->db->query($sql);
		$totalWorkOrder = $q->row_array();

		$count_result['total_workorder']['count'] = $totalWorkOrder['totalRecords'];
		$count_result['total_workorder']['url'] = 'Workorder';

		// get today update count
		$todaysUpdateCountSQL = "SELECT COUNT(id) AS totalRecords 
								FROM sites_tbl 
								WHERE DATE(update_date) = CURDATE()";
		$q = $this->db->query($todaysUpdateCountSQL);
		$res = $q->row_array(); 

		$count_result['today_update']['count'] = $res['totalRecords'];
		$count_result['today_update']['url'] = '';

		// echo "<pre>"; print_r($count_result); die;
		return $count_result;
	}

	public function getAreaManagerCounts()
	{	
		// define variables
		$count_result = [];
		$where1 = [];

		// get total projects
		// get project list
		$sql = "SELECT id FROM project_tbl";
		$q = $this->db->query($sql);
		$res = $q->result_array();

		$count_result['total_projects']['count'] = count($res);
		$count_result['total_projects']['url'] = 'Areamanager/Projects';

		//-- get completed projects
			// get all projects
		// echo "<pre>"; print_r($res); die;
		$totalCompletedProjects = 0;
		$totalOpenProjects = 0;

		if (count($res)>0) {
		
			foreach ($res as $key => $value) {

				// get total project sites
				$where1['project_id'] = $value['id'];
				$where1['area_manager'] = $this->user_id;
				$totalProjectSites = $this->Common_models->get_entry('sites_tbl', $where1, null, null, null, 0, 'id');
				$projectSitesID = array_column($totalProjectSites, 'id');
				$totalProjectSites = count($projectSitesID);
				// echo "<pre>"; print_r($projectSitesID); die;

				// get completed sites
				if (count($projectSitesID)>0) {
					$sel = "SELECT count(id) AS completedSites
							FROM contractor_execution 
							WHERE site_id IN (".implode(',', $projectSitesID).")";
					$q = $this->db->query($sel);
					$totalExecutedProjects = $q->row_array();
				} else {
					$totalExecutedProjects['completedSites'] = 0;
				}			

				if ($totalProjectSites != 0 && $totalProjectSites == $totalExecutedProjects['completedSites']) {
					$totalCompletedProjects++;
				} else {
					$totalOpenProjects++;
				}
			}
		}

		$count_result['total_completed_projects']['count'] = $totalCompletedProjects;
		$count_result['total_completed_projects']['url'] = 'Areamanager/Projects?filter=completeProject';

		$count_result['total_open_projects']['count'] = $totalOpenProjects;
		$count_result['total_open_projects']['url'] = 'Areamanager/Projects?filter=openProject';

		// today report count
		$todayReportCount = 0;
		if (count($res)>0) {

			$projectIDs = array_column($res, 'id');
			$sql = "SELECT COUNT(id) AS totalRecords 
					FROM project_tbl
					WHERE 
						DATE(update_date) = DATE(CURRENT_DATE) AND
						id IN (".implode(',', $projectIDs).") AND 
						id IN (SELECT project_id 
								FROM sites_tbl 
								WHERE area_manager = '$this->user_id'
								GROUP BY project_id)";
			$q = $this->db->query($sql);
			$todayReportCount = $q->row_array();
			$todayReportCount = $todayReportCount['totalRecords'];
		}

		$count_result['today_report']['count'] = $todayReportCount;
		$count_result['today_report']['url'] = 'Report?filter=todayUpdate';

		// get total work order
		$sql = "SELECT COUNT(wo.id) AS totalRecords 
				FROM workorder_tbl AS wo
				LEFT JOIN project_tbl 
					ON workorder_id = wo.id";
		$q = $this->db->query($sql);
		$totalWorkOrder = $q->row_array();

		$count_result['total_workorder']['count'] = $totalWorkOrder['totalRecords'];
		$count_result['total_workorder']['url'] = 'Workorder';

		// get today update count
		$todaysUpdateCountSQL = "SELECT COUNT(id) AS totalRecords 
								FROM sites_tbl 
								WHERE DATE(update_date) = CURDATE() AND 
									area_manager = '$this->user_id'";
		$q = $this->db->query($todaysUpdateCountSQL);
		$res = $q->row_array(); 

		$count_result['today_update']['count'] = $res['totalRecords'];
		$count_result['today_update']['url'] = 'Areamanager/Projects?filter=todayUpdate';

		// echo "<pre>"; print_r($count_result); die;
		return $count_result;
	}
}