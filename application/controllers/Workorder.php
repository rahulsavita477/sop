<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set("memory_limit", "-1");

class Workorder extends MY_Controller {

	function __construct() {
        parent::__construct();

        $this->userlogin_type = $this->session->userdata('ses_userlogin_type');
    	$this->user_id = $this->session->userdata('ses_userlogin_id');
    }

	public function index()
	{
		$where = array(
			'position_type='=>'project_head',
			'status'=>1
		);
		$data['project_heads'] = $this->Common_models->get_entry('admin_tbl', $where);
		$data['projects'] = $this->Common_models->get_entry('project_tbl');

		$sql = "SELECT wo.*, project_name
				FROM workorder_tbl AS wo
				LEFT JOIN project_tbl
					ON workorder_id = wo.id";

		if($this->userlogin_type != 'admin') {
			$sql .= " WHERE wo.created_by = ".$this->user_id;
		}

		$sql .= ' ORDER BY wo.update_date DESC';
		
		$q = $this->db->query($sql);
		$data['workorder_list'] = $q->result_array();

		$this->load->view('admin/common/header');
		$this->load->view('admin/workorder_view',$data);
		$this->load->view('admin/common/footer');
	}

	public function add_order() {

		$postdata = $this->input->post();
		
		$this->form_validation->set_rules('loi_number', 'LOI NO', 'trim');
		$this->form_validation->set_rules('loi_date', 'LOI date', 'trim');
		$this->form_validation->set_rules('workorder_no', 'Workorder Number', 'trim');
		$this->form_validation->set_rules('workorder_date', 'Workorder Date', 'trim');

		if($_POST['loi_number'] == '' && $_POST['workorder_no'] == ''){

			$this->form_validation->set_message('required', 'LOI or Workorder No is required!');
			$this->form_validation->set_rules('loi_number', 'Field 1', 'required');
		}

		if(!$this->form_validation->run()) {
			$this->session->set_flashdata('response','<div class="err_datasse">'.validation_errors().'</div>');
		} else {
			
			if($postdata['project_head']) {
				$project_head = implode(",",$postdata['project_head']);
			} else {
				$project_head = '';
			}

			$inserdata['loi_no'] = $postdata['loi_number'];
			$inserdata['loi_date'] = date('Y-m-d',strtotime($postdata['loi_date']));
			$inserdata['workorder_no'] = $postdata['workorder_no'];
			$inserdata['workorder_date'] = date('Y-m-d',strtotime($postdata['workorder_date']));
			$inserdata['order_deadline'] = date('Y-m-d',strtotime($postdata['order_deadline']));;
			$inserdata['project_head'] = $project_head;
			$inserdata['created_by'] = $this->session->userdata('ses_userlogin_id');
			$inserdata['create_date'] = date('Y-m-d H:i:s');
			
			$workorder_id = $this->Common_models->add_entry('workorder_tbl', $inserdata);
			$this->Common_models->update_entry(
				'project_tbl',
				['workorder_id' => $workorder_id],
				['id' => $postdata['project_id']]
			);
			
			$this->session->set_flashdata('response','<p class="alert alert-success">Success! Order added successfully.</p>');
		}
		
		return redirect('Workorder');
	}

	public function edit_order($id) {

		$where = array(
			'position_type=' => 'project_head',
			'status' => 1
		);
		$data['project_heads']=$this->Common_models->get_entry('admin_tbl', $where);
		
		$data['workorder_detail']=$this->Common_models->get_entry_row('workorder_tbl', ['id' => $id]);

		$data['projects'] = $this->Common_models->get_entry('project_tbl');
		// echo "<pre>"; print_r($data['projects']); die;

		$this->load->view('admin/common/header');
		$this->load->view('admin/edit_workorder',$data);
		$this->load->view('admin/common/footer');
	}

	public function do_editorder($id) {

		$postdata=$this->input->post();
		$project_head='';
		if($postdata['project_head']) {
			$project_head=implode(",",$postdata['project_head']);
		}

		$inserdata['loi_no'] = $postdata['loi_number'];
		$inserdata['loi_date'] = date('Y-m-d',strtotime($postdata['loi_date']));
		$inserdata['workorder_no'] = $postdata['workorder_no'];
		$inserdata['workorder_date'] = date('Y-m-d',strtotime($postdata['workorder_date']));
		$inserdata['order_deadline'] = $postdata['order_deadline'];
		$inserdata['project_head'] = $project_head;
		
		$this->Common_models->update_entry('workorder_tbl', $inserdata, array('id' => $id));
		$this->Common_models->update_entry(
			'project_tbl',
			['workorder_id' => $id],
			array('id' => $postdata['project_id'])
		);
		
		$this->session->set_flashdata('response','<p class="alert alert-success">Success! Order Updated successfully.</p>');

		// return redirect('Workorder/edit_order/'.$id);
		return redirect('Workorder');
	}

	public function order_received()
	{
		$user_id = $this->session->userdata('ses_userlogin_id');
		
		$sel = "SELECT workorder_tbl.*, project_tbl.workorder_id, project_tbl.id AS prjId 
				FROM workorder_tbl
				LEFT JOIN project_tbl 
					ON workorder_tbl.id = workorder_id AND project_tbl.created_by = '$user_id'
				WHERE FIND_IN_SET($user_id, project_head) 
				ORDER BY update_date DESC";

		$q=$this->db->query($sel);
		$aa=array();
		
		if($q->num_rows())
		{
			$aa=$q->result_array();
		}
		
		$data['workorder_list']=$aa;
		// echo "<pre>"; print_r($aa); die;
		
		$this->load->view('admin/common/header');
		$this->load->view('admin/order_received_view',$data);
		$this->load->view('admin/common/footer');
		
	}

	public function Projects() {

		$sel = "SELECT prj.*, order_deadline 
				FROM project_tbl AS prj
				LEFT JOIN workorder_tbl AS wo
					ON workorder_id = wo.id
				WHERE workorder_id IN (SELECT id 
									FROM workorder_tbl 
									WHERE created_by = '$this->user_id')";

		$q = $this->db->query($sel);
		$data['project_list'] = $q->result_array();

		$this->load->view('admin/common/header');
		$this->load->view('admin/Operationhead/projects',$data);
		$this->load->view('admin/common/footer');
	}

	public function sites($project_id) {

		// get circle list
		$data['circle_list'] = $this->Common_models->get_entry('sites_tbl', null, 'circle_name', 'ASC', null, 0, 'DISTINCT(circle_name)');

		// get district list
		$data['district_list'] = $this->Common_models->get_entry('sites_tbl', null, 'land_district', 'ASC', null, 0, 'DISTINCT(land_district)');

		// get taluka list
		$data['taluka_list'] = $this->Common_models->get_entry('sites_tbl', null, 'land_taluka', 'ASC', null, 0, 'DISTINCT(land_taluka)');

		// get village list
		$data['village_list'] = $this->Common_models->get_entry('sites_tbl', null, 'land_village', 'ASC', null, 0, 'DISTINCT(land_village)');

		$where1 = array(
			'project_id' => $project_id
		);

		if($this->userlogin_type != 'admin' && $this->userlogin_type != 'operation_head') {

			$where1['site_engineer'] = $this->session->userdata('ses_userlogin_id');
		}
		
		$data['sites_list'] = $this->Common_models->get_entry('sites_tbl', $where1);
		
		$data['project_detail'] = $this->Common_models->get_entry_row('project_tbl', array('id' => $project_id));
		// $data['isCompleted'] = $isCompleted;
		// echo "<pre>"; print_r($data); die;

		$this->load->view('admin/common/header');
		$this->load->view('admin/Operationhead/sites', $data);
		$this->load->view('admin/common/footer');
	}

	public function sites_server($project_id) {
		
		$user_id = $this->session->userdata('ses_userlogin_id');

		$where_string = '';
		if ($_GET['circle'] != 'null' && $_GET['circle'] != '0') {
			$where_string .= ' AND circle_name = "'.$_GET['circle'].'"';
		}
		
		if ($_GET['district'] != 'null' && $_GET['district'] != '0') {
			$where_string .= ' AND land_district = "'.$_GET['district'].'"';
		}
		
		if ($_GET['taluka'] != 'null' && $_GET['taluka'] != '0') {
			$where_string .= ' AND land_taluka = "'.$_GET['taluka'].'"';
		}

		if ($_GET['village'] != 'null' && $_GET['village'] != '0') {
			$where_string .= ' AND land_village = "'.$_GET['village'].'"';
		}

		if (isset($_GET['filter']) && $_GET['filter'] == 'todayUpdate') {
			$where_string .= ' AND DATE(update_date) = DATE(CURRENT_DATE)';
		}

		$get_data = $this->input->get();
		$start = $get_data['start'];
		$limit = $get_data['length'];

		$arrayList = [];
		
		// if ($isCompleted == 1) {
		// 	$contSQL = "SELECT COUNT(id) AS totalRecords 
		// 				FROM `sites_tbl` 
		// 				WHERE 
		// 					`project_id` = '$project_id' AND 
		// 					`site_engineer` = '$user_id' AND 
		// 					id IN (SELECT site_id 
		// 							FROM contractor_execution 
		// 							WHERE site_engineer_id = '$user_id')".
		// 					$where_string;

		// 	$sel = "SELECT * 
		// 			FROM `sites_tbl` 
		// 			WHERE 
		// 				`project_id` = '$project_id' AND 
		// 				`site_engineer` = '$user_id' AND 
		// 				id IN (SELECT site_id 
		// 						FROM contractor_execution 
		// 						WHERE site_engineer_id = '$user_id')".
		// 				$where_string."
		// 			ORDER BY id DESC 
		// 			LIMIT ".$start.", ".$limit;
		// } else {
			$contSQL = "SELECT COUNT(id) AS totalRecords 
						FROM `sites_tbl` 
						WHERE `project_id` = '$project_id'"; 
			
			if($this->userlogin_type == 'admin' || $this->userlogin_type == 'operation_head') {

			} else {

				$contSQL .= "AND `site_engineer` = '$user_id' AND
							id NOT IN (SELECT site_id 
										FROM contractor_execution 
										WHERE site_engineer_id = ".$user_id.")";
			}

			$contSQL .= $where_string;
			
			if($this->userlogin_type == 'admin' || $this->userlogin_type == 'operation_head') {

				$sel = "SELECT * 
						FROM `sites_tbl` 
						WHERE 
							`project_id` = '$project_id' ".
							$where_string."
						ORDER BY update_date DESC 
						LIMIT ".$start.", ".$limit;
			} else {

				$sel = "SELECT * 
						FROM `sites_tbl` 
						WHERE 
							`project_id` = '$project_id' AND 
							`site_engineer` = '$user_id' AND 
							id NOT IN (SELECT site_id 
									FROM contractor_execution 
									WHERE site_engineer_id = '$user_id') ".
							$where_string."
						ORDER BY update_date DESC 
						LIMIT ".$start.", ".$limit;
			}
		// }

		// echo $sel; die;
		// get total completed count
		$q = $this->db->query($contSQL);
		$res = $q->row_array();
		$recordsTotal = $res['totalRecords'];

		$q = $this->db->query($sel);
		$result = $q->result_array();

		$i=$this->input->get('start');
		
		foreach($result as $list) {

			if($this->userlogin_type != 'admin' && $this->userlogin_type != 'operation_head') {
				$where = array('site_id' => $list['id'], 'site_engineer_id' => $user_id);
			} else {
				$where = array('site_id' => $list['id']);
			}
			
			// check list id and user id is available in execution table or not
			$isSurveyUpdated = $this->Common_models->get_entry_row('site_survey', $where);
			
			// check list id and user id is available in survey table or not
			$isExecuted = $this->Common_models->get_entry_row('contractor_execution', $where);

			// if ($isCompleted == 1 && !$isExecuted) {
			// 	continue;
			// } else if ($isCompleted == 0 && $isExecuted) {
			// 	continue;
			// }

			$site_engineer=$area_manager=$contractor='';
			
			// check list id and user id is available in gather table or not
			if($this->userlogin_type != 'admin' && $this->userlogin_type != 'operation_head') {
				$where = array('id' => $list['id'], 'site_engineer' => $user_id);
			} else {
				$where = array('id' => $list['id']);
			}
			$isGathered = $this->Common_models->get_entry_row('sites_tbl', $where);
			if ($isGathered && count($isGathered) > 0) {
				$action='<a href="'.base_url('Siteengineer/edit_site/'.$list['id']).'" type="button" class="btn btn-block btn-danger">Gathering</a>';

				if (isset($isSurveyUpdated['id'])) {
					$action.='<a href="'.base_url('Siteengineer/start_survey/'.$list['id'].'/'.$project_id).'" type="button" class="btn btn-block btn-primary">Survey Done</a>';

					$action.='<a href="'.base_url('Product/site_product/'.$list['id'].'/'.$project_id).'" type="button" class="btn btn-block btn-default">Product & Material</a>';

					if (isset($isExecuted['id'])) {
						$isExecuted = 'Execution Done';
					} else {
						$isExecuted = 'Execution';
					}

					$action.='<a href="'.base_url('Siteengineer/contractor_execution/'.$list['id'].'/'.$project_id).'" type="button" class="btn btn-block btn-success">'.$isExecuted.'</a>';
				} else {
					$action.='<a href="'.base_url('Siteengineer/start_survey/'.$list['id'].'/'.$project_id).'" type="button" class="btn btn-block btn-primary">Survey update</a>';
				}
			} else {
				$action='<a href="'.base_url('Siteengineer/edit_site/'.$list['id']).'" type="button" class="btn btn-block btn-danger">Gathere</a>';
			}

			if($list['site_engineer'])
			{
				$whereoo=array('id'=>$list['site_engineer']);
				$enter_res=$this->Common_models->get_entry_row('admin_tbl',$whereoo);
				$site_engineer=$enter_res['name'];
			}

			if($list['area_manager'])
			{
				$whereoo=array('id'=>$list['area_manager']);
				$enter_res=$this->Common_models->get_entry_row('admin_tbl',$whereoo);
				$area_manager=$enter_res['name'];
			}

			if($list['contractor'])
			{
				$whereoo=array('id'=>$list['contractor']);
				$enter_res=$this->Common_models->get_entry_row('admin_tbl',$whereoo);
				$contractor=$enter_res['name'];
			}

			$arrayList [] = [
				++$i,
				$list['circle_name'],
				$list['land_district'],
				$list['land_village'],
				$list['land_taluka'],
				$list['workorder_no'],
				$list['beneficiary_id'],
				$list['beneficiary_name'],
				$list['mobilen_number'],
				$list['land_address'],
				$list['pump_load'],
				$list['category'],
				$list['work_order_date'],
				$list['application_status'],
				$list['installation_status'],
				$list['installation_date'],
				$list['remarks'],
				$list['lot'],
				$site_engineer,
				$area_manager,
				$contractor,
				$action
			];
		}

		$output = array(
			"draw" 				=> $this->input->get('draw'),
			"recordsTotal" 		=> $recordsTotal,
			"recordsFiltered"	=> $recordsTotal,
			"data" 				=> $arrayList,
		);
		echo json_encode($output);
	}	
}
