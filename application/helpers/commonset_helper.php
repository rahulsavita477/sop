<?php

function json_set($res) {

	$ci =& get_instance();

    $ci->output->set_content_type('application/json');
    $ci->output->_display(json_encode($res));

    exit(0);
}

function get_userby_id($id) {

	$ci =& get_instance();
	$userdetail = $ci->Common_models->get_entry_row('admin_tbl',array('id'=>$id));
	
	unset($userdetail['password']);

	return $userdetail;
}

function get_ICR_form_fields() {

	return [[
		"name" => "limeman_sign_date",
		"type" => "date",
		"title" => "Limeman Sign Date"
	],[
		"name" => "ae_je_sign_date",
		"type" => "date",
		"title" => "AR JE Sign Date"
	],[
		"name" => "inward_date",
		"type" => "date",
		"title" => "Inward Date"
	],[
		"name" => "ro_date",
		"type" => "date",
		"title" => "RO Date"
	],[
		"name" => "do_date",
		"type" => "date",
		"title" => "DO Date"
	],[
		"name" => "ho_date",
		"type" => "date",
		"title" => "HO Date"
	],[
		"name" => "act_dept_date",
		"type" => "date",
		"title" => "Act Dept Date"
	],[
		"name" => "se_tbl_date",
		"type" => "date",
		"title" => "SE Tbl Date"
	],[
		"name" => "moved_to_ho_date",
		"type" => "date",
		"title" => "Moved-To Date"
	],[
		"name" => "invoice_date",
		"type" => "date",
		"title" => "Invoice Date"
	],[
		"name" => "payment_advice_file",
		"type" => "file",
		"title" => "Payment Advice File"
	],[
		"name" => "hamipatra_file",
		"type" => "file",
		"title" => "Hamipatra File"
	],[
		"name" => "filled_signed_file",
		"type" => "file",
		"title" => "Filled Signed File"
	]];
}

function sendEmail($to='', $subject='', $message='', $atch='') {

    if (!$to || !$subject || !$message) {
		return false;
	}
    
    $ci =& get_instance();
    $ci->load->library('email');

    $config['protocol']  = EMAIL_PROTOCOL;
    $config['smtp_host'] = EMAIL_HOST;
    $config['smtp_port'] = EMAIL_PORT;
    $config['smtp_user'] = EMAIL_USERNAME;
    $config['smtp_pass'] = EMAIL_PASSWORD;
    $config['charset']   = "utf-8";
    $config['mailtype']  = "html";
    $config['newline']   = "\r\n";

    $ci->email->initialize($config);
    $ci->email->from(EMAIL_ID, EMAIL_NAME);
    $ci->email->to($to);
    $ci->email->reply_to(EMAIL_ID, EMAIL_NAME);
    $ci->email->subject(ucfirst($subject));
    $ci->email->message($message);

    if ($atch) {
        $ci->email->attach($atch);
	}

    if($ci->email->send()) {
        return true;
	} else {
        return false;
	}
}

function accessLavels($role) {

	$a_accessLavels = [];

	switch ($role) {

		case ADMIN:
			$a_accessLavels = [
				ADMIN => 'Admin',
				OPERATION_HEAD => 'Operation Head',
				PROJECT_HEAD => 'Project Head',
				PROJECT_MANAGER => 'Project Manager',
				SITE_ENGINEER => 'Site Engineer',
				AREA_MANAGER => 'Area Manager',
				CONTRACTOR => 'Contractor',
				LOGISTIC_HEAD => 'Logistic Head',
				ACCOUNT_HEAD => 'Account Head'
			];
		break;

		case OPERATION_HEAD:
			$a_accessLavels = [
				PROJECT_HEAD => 'Project Head',
				PROJECT_MANAGER => 'Project Manager',
				SITE_ENGINEER => 'Site Engineer',
				AREA_MANAGER => 'Area Manager',
				CONTRACTOR => 'Contractor',
				LOGISTIC_HEAD => 'Logistic Head',
				ACCOUNT_HEAD => 'Account Head'
			];
		break;

		case PROJECT_HEAD:
			$a_accessLavels = [
				PROJECT_MANAGER => 'Project Manager',
				SITE_ENGINEER => 'Site Engineer',
				AREA_MANAGER => 'Area Manager',
				CONTRACTOR => 'Contractor',
				LOGISTIC_HEAD => 'Logistic Head',
				ACCOUNT_HEAD => 'Account Head'
			];
		break;

		case PROJECT_MANAGER:
			$a_accessLavels = [
				SITE_ENGINEER => 'Site Engineer',
				AREA_MANAGER => 'Area Manager',
				CONTRACTOR => 'Contractor',
				LOGISTIC_HEAD => 'Logistic Head',
				ACCOUNT_HEAD => 'Account Head'
			];
		break;

		case SITE_ENGINEER:
			$a_accessLavels = [
				AREA_MANAGER => 'Area Manager',
				CONTRACTOR => 'Contractor',
				LOGISTIC_HEAD => 'Logistic Head',
				ACCOUNT_HEAD => 'Account Head'
			];
		break;

		case AREA_MANAGER:
			$a_accessLavels = [
				SITE_ENGINEER => 'Site Engineer',
				CONTRACTOR => 'Contractor',
				LOGISTIC_HEAD => 'Logistic Head',
				ACCOUNT_HEAD => 'Account Head'
			];
		break;

		case CONTRACTOR:
			$a_accessLavels = [LOGISTIC_HEAD => 'Logistic Head', ACCOUNT_HEAD => 'Account Head'];
		break;

		case LOGISTIC_HEAD:
			$a_accessLavels = [ACCOUNT_HEAD => 'Account Head'];
		break;

		case ACCOUNT_HEAD:
			$a_accessLavels = [];
		break;

		default:
			$a_accessLavels = [];
		break;
	}

	return $a_accessLavels;
}
